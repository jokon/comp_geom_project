#!/bin/bash
set -e

if [ "$(lsb_release -si)" == 'Ubuntu' ]; then
	sudo apt-get install -y qt5-default
	sudo apt-get install -y libboost-all-dev
	sudo apt-get install -y doxygen
	sudo apt-get install -y cmake
	sudo apt-get install -y gcc
	sudo apt-get install -y g++
	sudo apt-get install -y git
else
	echo "You will have to install the following dependencies manually:"
	echo "    gcc g++ git make cmake qt5-base-dev libboust-dev doxygen"
fi

# Clone repository
rm -rf /tmp/VVRFramework
git clone --recursive --depth=1 https://bitbucket.org/vvr/vvrframework.git /tmp/VVRFramework

# Build and install
cmake /tmp/VVRFramework/CMakeLists.txt
cd /tmp/VVRFramework && make
cd /tmp/VVRFramework && sudo make install

# Clean up
rm -rf /tmp/VVRFramework

