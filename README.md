# 3D Mesh Processing Tool

This project was implemented for the Computational Geometry and 3D Applications course, taught by Prof. Konstantinos Moustakas in the University of Patras, Greece

#### Installation

First you need to install [VVR Framework](https://github.com/chrispap/VVRFramework/tree/master/3rdParty). This project has only been tested in Windows systems and specifically by using Visual Studio 2010. 


#### Features of this project

__1)__ Create the Dual Graph of a mesh. The Dual Graph is created by connecting the centroids of adjacent triangles. The Dual Graph is shown in cyan colour in the images below.

<img src="./images/image-001.png" width="30%"> <img src="./images/image-000.png" width="43%">  

__2)__ Select a triangle of a mesh by clicking on it.

<img src="./images/image-004.png" width="30%">

__3)__ Select a triangle and find its adjacent triangles up to degree N.

- degree N = 4

<img src="./images/image-005.png" width="30%">

__4)__ Subdivide each triangle of the selected region into multiple triangles K times. The two parameters that need to be defined are N (degree of adjacency) and K. Some triangles outside the selected region need to be subdivided as well, so as for the application to be able to create the Dual Graph of the modified mesh.

- N = 2 and K = 1

<img src="./images/image-011.png" width="30%">

- N = 2 and K = 2

<img src="./images/image-012.png" width="30%">


__5)__ Find the mean normal vector of a mesh and create a plane defined by that normal vector. This plane subdivides the vertices of the mesh into two regions. 

<img src="./images/image-013.png" width="30%"> <img src="./images/image-015.png" width="22%">

#### License

3D Mesh Processing  is free for non-commercial use.
Copyright  © 2016 Ioanna Kontou

3D Mesh Processing uses the [VVR Framework](https://bitbucket.org/vvr/vvrframework) and the symmetriceigensolver3x3 library.


