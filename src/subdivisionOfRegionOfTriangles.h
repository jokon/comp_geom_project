/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/
#ifndef _SUBDIVISION_TRIANGLES 
#define _SUBDIVISION_TRIANGLES

#include "adjacencyOfGradeN.h"
#include "adjacencyOfTriRegion.h"


class Processing_of_triangles_tool
{
	//private fields
	int grade_of_subdivision;
	std::vector<int> mesh_indices_of_tris_to_be_processed;
	std::vector<int> mesh_indices_of_verts_of_tris_to_be_processed; //--> for every tri to be processed, 3 indices correspond to its vertices
    std::vector<math::float3> new_vector_of_mesh_vertices;

	//new vectors i add for subdivision of outer boundary triangles
	std::vector<int> mesh_indices_of_outer_bound_tris_to_be_processed;
	std::vector<int> mesh_indices_of_verts_of_outer_bound_tris_to_be_processed;
	std::vector<Bound_tris_adj_1> data_type_1;
	std::vector<Bound_tris_adj_2> data_type_2;
	
	//methods
private:
	void get_grade_of_subdivision();
	void subdivide_triangle(int tri_v1, int tri_v2, int tri_v3, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &m_pts);
	void find_midpoint_of_segment(math::float3 v1, math::float3 v2, math::float3 &mid_pt);
	void make_the_appropriate_modifications_to_vectors_with_data_of_subdivision(int &init, int index_of_tri_subdivided, std::vector<math::float3> mid_pts, std::vector<math::Triangle> new_tris, vvr::Mesh mesh);
	bool vert_already_exists_to_verts_vector(math::float3 v, int init_size_of_mesh_verts_vector);
	void find_indices_of_triangle(math::Triangle tri, std::vector<int> &v);
	void make_updates_needed_to_mesh_vectors(vvr::Mesh &mesh);
	void subdivide_inner_region(int init, vvr::Mesh mesh);
	//
	void subdivide_outer_bound_tri(int vi1, int vi2, int vi3, int outer_vector_index, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &mid_pt);
	void subdivide_outer_bound_tri_type_1(int vi1, int vi2, int vi3, int outer_vector_index, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &mid_pt);
	void subdivide_outer_bound_tri_type_2(int vi1, int vi2, int vi3, int outer_vector_index, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &mid_pt);
	void make_the_appropriate_modifications_to_vectors_with_data_of_subdivision_for_outer_boundary_tris(int &init,int index_of_vert_to_verts_vector, std::vector<math::float3> mid_pts, std::vector<math::Triangle> new_tris, vvr::Mesh mesh);
	void subdivide_outer_boundary_tri_region(int &init, vvr::Mesh mesh);
public:
	Processing_of_triangles_tool(Adjacency_of_triangle *adj_of_tri, Adjacency_of_tri_region *adj_of_tri_reg, vvr::Mesh mesh);
	void subdivide_region_of_tris_of_mesh(vvr::Mesh &mesh, int init1, int &init2, Dual_Graph *dual_graph, Adjacency_of_tri_region *adjac_of_tri_reg, Adjacency_of_triangle *adj_of_tri);
	void draw(vvr::Mesh mesh);

};

void Processing_of_triangles_tool::get_grade_of_subdivision()
{
	std::cout << "Type the grade of subdivision of region of triangles...";
	int n;
	std::cin >> n;
	while(n < 0)
	{
		std::cout << "Number must be an integer! Type again..";
		std::cin >> n;
	}
	grade_of_subdivision = n;
}


Processing_of_triangles_tool::Processing_of_triangles_tool(Adjacency_of_triangle * adj_of_tri, Adjacency_of_tri_region * adj_of_tri_reg, vvr::Mesh mesh)
{
	//initializations

	//1)grade
	get_grade_of_subdivision();
	
	//2)mesh_indices_of_tris_to_be_processed
	mesh_indices_of_tris_to_be_processed.clear();
	adj_of_tri ->get_indices_of_adjac_tris(mesh_indices_of_tris_to_be_processed);

	//3)mesh_indices_of_verts_of_tris_to_be_processed
		
	mesh_indices_of_verts_of_tris_to_be_processed.clear();
	for(unsigned int i = 0 ; i < mesh_indices_of_tris_to_be_processed.size() ; i++)
	{
		mesh_indices_of_verts_of_tris_to_be_processed.push_back((mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].vi1);
		mesh_indices_of_verts_of_tris_to_be_processed.push_back((mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].vi2);
		mesh_indices_of_verts_of_tris_to_be_processed.push_back((mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].vi3);
	}
	
	//4)new_vector_of_mesh_vertices
	new_vector_of_mesh_vertices.clear();
	new_vector_of_mesh_vertices = mesh.getVertices();
	
	//5)data_type_1, data_type_2 , temp3(type3_indices)
	std::vector<int> temp1(0), temp2(0), temp3(0);
	data_type_1.clear();
	data_type_2.clear();
	adj_of_tri_reg->get_type_1_tris_info_vectors(data_type_1, temp1);
	adj_of_tri_reg->get_type_2_tris_info_vectors(data_type_2, temp2);
	adj_of_tri_reg->get_type_3_tris_indices(temp3);


	//6)mesh_indices_of_outer_bound_tris_to_be_processed
	//merge temp1,temp2, temp3
	mesh_indices_of_outer_bound_tris_to_be_processed.clear();
	for(unsigned int i = 0 ; i < temp1.size() ; i++)
		mesh_indices_of_outer_bound_tris_to_be_processed.push_back(temp1[i]);

	for(unsigned int i = 0 ; i < temp2.size() ; i++)
		mesh_indices_of_outer_bound_tris_to_be_processed.push_back(temp2[i]);

	for(unsigned int i = 0 ; i < temp3.size() ; i++)
		mesh_indices_of_outer_bound_tris_to_be_processed.push_back(temp3[i]);

	//7)mesh_indices_of_verts_of_outer_bound_tris_to_be_processed;
	mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.clear();
	std::vector<vvr::Triangle> tris = mesh.getTriangles();
	for(unsigned int i = 0 ; i < mesh_indices_of_outer_bound_tris_to_be_processed.size() ; i++)
	{
		mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(tris[mesh_indices_of_outer_bound_tris_to_be_processed[i]].vi1);
		mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(tris[mesh_indices_of_outer_bound_tris_to_be_processed[i]].vi2);
		mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(tris[mesh_indices_of_outer_bound_tris_to_be_processed[i]].vi3);
	}
	
}

void Processing_of_triangles_tool::find_midpoint_of_segment(math::float3 v1, math::float3 v2, math::float3 &mid_pt)
{
	mid_pt.x = (v1.x + v2.x)/2;
	mid_pt.y = (v1.y + v2.y)/2;
	mid_pt.z = (v1.z + v2.z)/2;
}

void Processing_of_triangles_tool::subdivide_triangle(int vi1, int vi2, int vi3, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &m_pts)
{
	//subdivide a triangle (v1, v2, v3) into 4 triangles by using mid-points of sides of triangle 
	//m1 is the mid point of (v2, v3) side
	//m2 is the mid point of (v3, v1) side
	//m3 is the mid point of (v1, v2) side
	//so (v1, v2, v3) is subdivided into (v1, m3, m2), (m3, v2, m1), (m1, m2, m3), (m1, v3, m2)
	//i assume that v1, v2, v3 are in ccw order

	//Step 1 : find the vertices of triangle
	float3 vec1 = new_vector_of_mesh_vertices[vi1];
	float3 vec2 = new_vector_of_mesh_vertices[vi2];
	float3 vec3 = new_vector_of_mesh_vertices[vi3];

	//Step 2 : find mid points of sides of triangle
	float3 m1, m2, m3;

	find_midpoint_of_segment(vec1, vec2, m3);
	find_midpoint_of_segment(vec2, vec3, m1);
	find_midpoint_of_segment(vec3, vec1, m2);

	//Step 3 : push midpoints to m_pts vector
	m_pts.push_back(m1);
	m_pts.push_back(m2);
	m_pts.push_back(m3);

	//Step 4 : create the 4 new triangles and push them back to new_tris vector
	new_tris.push_back(math::Triangle(vec1, m3, m2));
	new_tris.push_back(math::Triangle(m3, vec2, m1));
	new_tris.push_back(math::Triangle(m1, m2, m3));
	new_tris.push_back(math::Triangle(m1, vec3, m2));

}

void Processing_of_triangles_tool::subdivide_outer_bound_tri_type_1(int vi1, int vi2, int vi3, int outer_vector_index, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &mid_pt)
{
	//Step 1 : find the vertices of triangle
	float3 v1 = new_vector_of_mesh_vertices[vi1];
	float3 v2 = new_vector_of_mesh_vertices[vi2];
	float3 v3 = new_vector_of_mesh_vertices[vi3];

	//Step 2 : find indices of common vertices of outer boundary triangle with inner boundary tri
	int com_vi1 = data_type_1[outer_vector_index].common_vi1;
	int com_vi2 = data_type_1[outer_vector_index].common_vi2;
	
	//Step 3 : find common vertices of outer boundary triangle with inner tri of region
	float3 com_v1 = new_vector_of_mesh_vertices[com_vi1];
	float3 com_v2 = new_vector_of_mesh_vertices[com_vi2];

	//Step 4 : find adjacent vertex to common segment
	int adj_vi = data_type_1[outer_vector_index].adj_to_com_seg;
	float3 adj_v = new_vector_of_mesh_vertices[adj_vi];

	//Step 5 : find mid_pt of common segment
	math::vec m_pt;
	find_midpoint_of_segment(com_v1, com_v2, m_pt);
	mid_pt.push_back(m_pt);

	

	//Step 6 : find the verts of tris divided in counter-clock wise order
	//each tri of type 1 is divided into two new tris
	if((com_v1.Equals(v1) && com_v2.Equals(v2)) || (com_v1.Equals(v2) && com_v2.Equals(v3)) || (com_v1.Equals(v3) && com_v2.Equals(v1)))
	{
		new_tris.push_back(math::Triangle(mid_pt[0], adj_v, com_v1));
		new_tris.push_back(math::Triangle(mid_pt[0], com_v2, adj_v));
	}
	else if((com_v2.Equals(v1) && com_v2.Equals(v3)) || (com_v1.Equals(v2) && com_v2.Equals(v1)) || (com_v1.Equals(v3) && com_v2.Equals(v2)))
	{
		new_tris.push_back(math::Triangle(mid_pt[0], com_v1, adj_v));
		new_tris.push_back(math::Triangle(mid_pt[0], adj_v, com_v2));
	}

}


void Processing_of_triangles_tool::subdivide_outer_bound_tri_type_2(int vi1, int vi2, int vi3, int outer_vector_index, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &mid_pt)
{
	//Step 1 : find the vertices of triangle
	float3 v1 = new_vector_of_mesh_vertices[vi1];
	float3 v2 = new_vector_of_mesh_vertices[vi2];
	float3 v3 = new_vector_of_mesh_vertices[vi3];

	//Step 2 : find indices of common vertices of outer boundary triangle with inner boundary tri
	int com_vi1 = data_type_2[outer_vector_index - data_type_1.size()].common_vi1;
	int com_vi2 = data_type_2[outer_vector_index - data_type_1.size()].common_vi2;
	int com_vi3 = data_type_2[outer_vector_index - data_type_1.size()].common_vi3;
	int com_vi4 = data_type_2[outer_vector_index - data_type_1.size()].common_vi4;

	//Step 3 : find index of common vertex of two inner boundary tris with outer bound tri 
	int common_pt_index;
	if((com_vi1 == com_vi3) || (com_vi1 == com_vi4))//com_vi1 != com_vi2 and com_vi3 != com_vi4 by default
		common_pt_index = com_vi1;
	else if((com_vi2 == com_vi3) || (com_vi2 == com_vi4))//////////make the comparisons with com_vi2 if sth goes wrong/////erase it afterwards////
		common_pt_index = com_vi2;

	//Step 4 : save inds of verts of tri to a vector
	std::vector<int> verts_ind(0);
	verts_ind.push_back(vi1);
	verts_ind.push_back(vi2);
	verts_ind.push_back(vi3);

	//i suppose that com_index has index i to verts_ind 
	//to access the element that is before it, verts_ind[(i-1) % 3] is used
	//to access the proceeding element verts_ind[(i + 1) % 3] is used  
	//so

	//Step 5 : find common_pt_index's index to verts_inds
	int index;
	if(common_pt_index == verts_ind[0])
		index = 0;
	else if(common_pt_index == verts_ind[1])
		index = 1;
	else
		index = 2;

	//Step 5 : find mid points of common segments
	math::float3 mid_pt1, mid_pt2;
	find_midpoint_of_segment(new_vector_of_mesh_vertices[verts_ind[index]], new_vector_of_mesh_vertices[verts_ind[((index - 1) % 3 + 3)%3]], mid_pt1);
	find_midpoint_of_segment(new_vector_of_mesh_vertices[verts_ind[index]], new_vector_of_mesh_vertices[verts_ind[(index + 1) % 3]], mid_pt2);

	mid_pt.push_back(mid_pt1);
	mid_pt.push_back(mid_pt2);

	//the initial triangle of type 2 is going to be subdivided into 3 new triangles
	//Step 6 : push_back new triangles (every tri is in ccw order) to new_tris vector
	new_tris.push_back(math::Triangle(mid_pt1, new_vector_of_mesh_vertices[common_pt_index], mid_pt2));
	new_tris.push_back(math::Triangle(mid_pt1, mid_pt2, new_vector_of_mesh_vertices[verts_ind[((index - 1) % 3 + 3)% 3]]) );
	new_tris.push_back(math::Triangle(new_vector_of_mesh_vertices[verts_ind[((index - 1) % 3 + 3) % 3]], mid_pt2, new_vector_of_mesh_vertices[verts_ind[(index + 1) % 3]]));
}

void Processing_of_triangles_tool::subdivide_outer_bound_tri(int vi1, int vi2, int vi3, int outer_vector_index, std::vector<math::Triangle> &new_tris, std::vector<math::float3> &mid_pt)
{
	// mesh_indices_of_outer_bound_tris_to_be_processed is a result of merge of indices of tris type 1 and tris type2
	//indices of tris type 1 are pushed first, so outer_vector_indices that are smaller of data_type_1 size reffer to tris type 1
	if(outer_vector_index < data_type_1.size())
		//TYPE 1 
		subdivide_outer_bound_tri_type_1(vi1, vi2, vi3, outer_vector_index, new_tris, mid_pt);
	else if(outer_vector_index < data_type_1.size() + data_type_2.size())
		//TYPE 2
		subdivide_outer_bound_tri_type_2(vi1, vi2, vi3,outer_vector_index, new_tris, mid_pt);
	else
		//TYPE 3
		subdivide_triangle(vi1, vi2, vi3, new_tris, mid_pt);
}

void Processing_of_triangles_tool::subdivide_outer_boundary_tri_region(int &init, vvr::Mesh mesh)
{
	std::vector<int> vec_inds = mesh_indices_of_verts_of_outer_bound_tris_to_be_processed;
    unsigned int vec_size = mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.size();
	for(unsigned int i = 0 ; i < vec_size ; i+= 3)
	{
		//Step 1 : subdivide tri

		std::vector<math::Triangle> new_tris(0);
		std::vector<math::vec> mid_pts(0);
		subdivide_outer_bound_tri(vec_inds[i], vec_inds[i + 1], vec_inds[i +2], i / 3, new_tris, mid_pts);

		//Step 2 : make appropriate modifications to vectors of outer boundary tris data
		make_the_appropriate_modifications_to_vectors_with_data_of_subdivision_for_outer_boundary_tris(init, i, mid_pts, new_tris, mesh);
	}


}

bool Processing_of_triangles_tool::vert_already_exists_to_verts_vector(math::float3 v, int init_size_of_mesh_verts_vector)
{
	bool exists = false;
	
	for(unsigned int k = init_size_of_mesh_verts_vector ; k < new_vector_of_mesh_vertices.size() ; k++)
	{
		if(v.Equals(new_vector_of_mesh_vertices[k]))
		{
			exists = true;
			break;
		}
	}

	if(exists)
		return true;
	else
		return false;
}


void Processing_of_triangles_tool::find_indices_of_triangle(math::Triangle tri, std::vector<int> &v)
{
	for(unsigned int a = 0 ; a < 3 ; a++)
	{
		math::vec ver = tri.Vertex(a);
		for(int b = 0; b < new_vector_of_mesh_vertices.size() ; b++)
		{
			if(ver.Equals(new_vector_of_mesh_vertices[b]))
			{
				v.push_back(b);
				break;
			}	
		}
	}
}

void Processing_of_triangles_tool::make_the_appropriate_modifications_to_vectors_with_data_of_subdivision_for_outer_boundary_tris(int &init,int index_of_vert_to_verts_vector, std::vector<math::float3> mid_pts, std::vector<math::Triangle> new_tris, vvr::Mesh mesh)
{
	//Step 2 : find the indices of vertices of the triangle new_tris[h]
	std::vector<int> inds(0);
	for(unsigned int h = 0 ; h < new_tris.size() ; h++)
		find_indices_of_triangle(new_tris[h], inds);

	//Step 3 : for every tri of the new ones  make updates to vector: 
	//1)mesh_indices_of_verts_of_outer_bound_tris_to_be_processed 
	mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[index_of_vert_to_verts_vector] = inds[0];
	mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[index_of_vert_to_verts_vector + 1] = inds[1];
	mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[index_of_vert_to_verts_vector + 2] = inds[2];

	if(index_of_vert_to_verts_vector / 3 < data_type_1.size())
	{
		for(unsigned int h = 3 ; h < 6 ; h++)
			mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(inds[h]);
	}
	else if (index_of_vert_to_verts_vector / 3 < data_type_1.size() + data_type_2.size())
	{
		for(unsigned int h = 3 ; h < 9 ; h++)
			mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(inds[h]);
	}else
		for(unsigned int h = 3 ; h < 12 ; h++)
			mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(inds[h]);

	//2)mesh_indices_of_outer_bound_tris_to_be_processed
	for(unsigned int j = 0 ; j < (new_tris.size() - 1) ; j++)
	{
		if(init == 0)
		{
			init = 1;
			mesh_indices_of_outer_bound_tris_to_be_processed.push_back(mesh_indices_of_tris_to_be_processed[mesh_indices_of_tris_to_be_processed.size() - 1] + 1);
		}
		else
			mesh_indices_of_outer_bound_tris_to_be_processed.push_back(mesh_indices_of_outer_bound_tris_to_be_processed[mesh_indices_of_outer_bound_tris_to_be_processed.size() - 1] + 1);
	}


}

void Processing_of_triangles_tool::make_the_appropriate_modifications_to_vectors_with_data_of_subdivision(int &init, int index_of_vert_to_verts_vector, std::vector<math::float3> mid_pts, std::vector<math::Triangle> new_tris, vvr::Mesh mesh)
{
	//Step 1 : check if m_pts[h] of tri already exists in new_vector_of_mesh_vertices
	//if not add it to new_vector_of_mesh_vertices,
	for(unsigned int h = 0 ; h < 3 ; h++)
	{
		if(!vert_already_exists_to_verts_vector(mid_pts[h], mesh.getVertices().size()))
			new_vector_of_mesh_vertices.push_back(mid_pts[h]);
	}
	 
	//Step 2 : find the indices of vertices of the triangle new_tris[h]
	std::vector<int> inds(0);
	for(unsigned int h = 0 ; h < 4 ; h++)
		find_indices_of_triangle(new_tris[h], inds);

	//Step 3 : for every tri of the four ones  make updates to vector: 
	//1)mesh_indices_of_verts_of_tris_to_be_processed 
	mesh_indices_of_verts_of_tris_to_be_processed[index_of_vert_to_verts_vector] = inds[0];
	mesh_indices_of_verts_of_tris_to_be_processed[index_of_vert_to_verts_vector + 1] = inds[1];
	mesh_indices_of_verts_of_tris_to_be_processed[index_of_vert_to_verts_vector + 2] = inds[2];

	for(unsigned int h = 3 ; h < 12 ; h++)
		 mesh_indices_of_verts_of_tris_to_be_processed.push_back(inds[h]);

	//2)mesh_indices_of_tris_to_be_processed
	for(unsigned int j = 0 ; j < 3 ; j++)
	{
		if(init == 0)
		{
			init = 1;
			mesh_indices_of_tris_to_be_processed.push_back(mesh.getTriangles().size());
		}
		else
			mesh_indices_of_tris_to_be_processed.push_back(mesh_indices_of_tris_to_be_processed[mesh_indices_of_tris_to_be_processed.size() - 1] + 1);
	}
}

void Processing_of_triangles_tool::subdivide_inner_region(int init, vvr::Mesh mesh)
{
	int size = mesh_indices_of_verts_of_tris_to_be_processed.size();
		//for every tri that has to be subdivided
		for(unsigned int q = 0 ; q < size ; q+=3)
		{	
			std::vector<math::Triangle> new_tris(0);
			std::vector<math::float3> mid_pts(0);
			//Step 2 : find the 4 tris that the tri is divided to
			subdivide_triangle(mesh_indices_of_verts_of_tris_to_be_processed[q], mesh_indices_of_verts_of_tris_to_be_processed[q + 1], mesh_indices_of_verts_of_tris_to_be_processed[q + 2], new_tris, mid_pts);
			//Step 3 : make the modifications to vectors-fields of Processing_of_triangles_tool class
			make_the_appropriate_modifications_to_vectors_with_data_of_subdivision(init, q, mid_pts,new_tris, mesh);
			
		}
}


void Processing_of_triangles_tool::make_updates_needed_to_mesh_vectors(vvr::Mesh &mesh)
{
	//Step 1 : push back to vector mesh.getVertices() the new mid_points
	for(unsigned int i = mesh.getVertices().size() ; i < new_vector_of_mesh_vertices.size() ; i++)
		mesh.getVertices().push_back(new_vector_of_mesh_vertices[i]);

	//Step 2 : update the vector mesh.getTriangles() with the new triangles
	int size = mesh.getTriangles().size();

	//inner triangles subdivision updates
	for(unsigned int i = 0 ; i < mesh_indices_of_tris_to_be_processed.size() ; i++)
	{
		if(mesh_indices_of_tris_to_be_processed[i] < size)
			mesh.getTriangles()[mesh_indices_of_tris_to_be_processed[i]] = vvr::Triangle(&(mesh.getVertices()), mesh_indices_of_verts_of_tris_to_be_processed[3 * i], mesh_indices_of_verts_of_tris_to_be_processed[3 * i + 1], mesh_indices_of_verts_of_tris_to_be_processed[3 * i + 2]);
		if(mesh_indices_of_tris_to_be_processed[i] >= size)
			mesh.getTriangles().push_back(vvr::Triangle(&(mesh.getVertices()), mesh_indices_of_verts_of_tris_to_be_processed[3 * i], mesh_indices_of_verts_of_tris_to_be_processed[3 * i + 1], mesh_indices_of_verts_of_tris_to_be_processed[3 * i + 2]));
	}

	//outer boundary triangles subdivision updates
	for(unsigned int i = 0 ; i < mesh_indices_of_outer_bound_tris_to_be_processed.size() ; i++)
	{
		if(mesh_indices_of_outer_bound_tris_to_be_processed[i] < size)
			mesh.getTriangles()[mesh_indices_of_outer_bound_tris_to_be_processed[i]] = vvr::Triangle(&(mesh.getVertices()), mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[3 * i], mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[3 * i + 1], mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[3 * i + 2]);
		if(mesh_indices_of_outer_bound_tris_to_be_processed[i] >= size)
			mesh.getTriangles().push_back(vvr::Triangle(&(mesh.getVertices()), mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[3 * i], mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[3 * i + 1], mesh_indices_of_verts_of_outer_bound_tris_to_be_processed[3 * i + 2]));
	}
	mesh.update();
}


void Processing_of_triangles_tool::subdivide_region_of_tris_of_mesh(vvr::Mesh &mesh, int init1, int &init2, Dual_Graph *dual_graph, Adjacency_of_tri_region *adjac_of_tri_reg, Adjacency_of_triangle *adj_of_tri)
{

	
	//Step 1 : do the subdivision as many times as grade_of_subdivision indicates
	for(unsigned int m = 0 ; m < grade_of_subdivision ; m++)
	{
		init2 = 0, init1 = 0;
		std::cout << mesh.getTriangles().size() << std::endl;
		//Step 2 : subdivide inner region
		subdivide_inner_region(init1, mesh);

		//Step 3 : subdivide outer boundary region triangles
		subdivide_outer_boundary_tri_region(init2, mesh);

		//Step 4 : make updates needed to mesh
		make_updates_needed_to_mesh_vectors(mesh);///////////////na veltiiwsw thn apodosh tou-->ligoteres sugkriseis
		
		//4)new_vector_of_mesh_vertices
		//new_vector_of_mesh_vertices.clear();
		//new_vector_of_mesh_vertices = mesh.getVertices();
		
		
		//find adjacency from dual graph
		dual_graph->find_dual_graph(mesh.getTriangles());
		
		
		//find boundary_adjacency info from adjacency_of_tri_region_tool
		adjac_of_tri_reg -> set_mesh_inds_vector_of_regional_tris(mesh_indices_of_tris_to_be_processed);
		///////////////////////////////
	

		adjac_of_tri_reg->handle_adjacency_of_boundary_tris(mesh, dual_graph);
	//	adjac_of_tri_reg ->mesh_indices_of_regional_triangles;
		/////////////////
		//adjac_of_tri_reg -> handle_adjacency_of_boundary_tris(mesh, dual_graph);

		///////////////////////////////////////
		//5)data_type_1, data_type_2 , temp3(type3_indices)
		std::vector<int> temp1(0), temp2(0), temp3(0);
		data_type_1.clear();
		data_type_2.clear();
		adjac_of_tri_reg->get_type_1_tris_info_vectors(data_type_1, temp1);
		adjac_of_tri_reg->get_type_2_tris_info_vectors(data_type_2, temp2);
		adjac_of_tri_reg->get_type_3_tris_indices(temp3);


		//6)mesh_indices_of_outer_bound_tris_to_be_processed
		//merge temp1,temp2, temp3
		mesh_indices_of_outer_bound_tris_to_be_processed.clear();
		for(unsigned int i = 0 ; i < temp1.size() ; i++)
			mesh_indices_of_outer_bound_tris_to_be_processed.push_back(temp1[i]);

		for(unsigned int i = 0 ; i < temp2.size() ; i++)
			mesh_indices_of_outer_bound_tris_to_be_processed.push_back(temp2[i]);

		for(unsigned int i = 0 ; i < temp3.size() ; i++)
			mesh_indices_of_outer_bound_tris_to_be_processed.push_back(temp3[i]);

		//7)mesh_indices_of_verts_of_outer_bound_tris_to_be_processed;
		mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.clear();
		std::vector<vvr::Triangle> tris = mesh.getTriangles();
		for(unsigned int i = 0 ; i < mesh_indices_of_outer_bound_tris_to_be_processed.size() ; i++)
		{
			mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(tris[mesh_indices_of_outer_bound_tris_to_be_processed[i]].vi1);
			mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(tris[mesh_indices_of_outer_bound_tris_to_be_processed[i]].vi2);
			mesh_indices_of_verts_of_outer_bound_tris_to_be_processed.push_back(tris[mesh_indices_of_outer_bound_tris_to_be_processed[i]].vi3);
		}
		adj_of_tri->set_adjacency_of_tri_region(mesh_indices_of_tris_to_be_processed);
	/////////////////////////////////////
	}
	
	

}



void Processing_of_triangles_tool::draw(vvr::Mesh mesh)
{
	for(unsigned int i = 0 ; i < mesh_indices_of_tris_to_be_processed.size() ; i++)
	{
		vvr::Triangle3D tri( (mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v1().x,(mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v1().y,(mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v1().z, (mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v2().x,(mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v2().y,(mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v2().z, (mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v3().x, (mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v3().y, (mesh.getTriangles())[mesh_indices_of_tris_to_be_processed[i]].v3().z, vvr::Colour::darkOrange);
		tri.draw();
	}

}

#endif
