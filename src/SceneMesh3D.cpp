/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/
#include "SceneMesh3D.h"


using namespace std;
using namespace vvr;


Mesh3DScene::Mesh3DScene()
{
    //! Load settings.
    vvr::Shape::DEF_LINE_WIDTH = 4;
    vvr::Shape::DEF_POINT_SIZE = 10;
    m_perspective_proj = true;
    m_bg_col = Colour("768E77");
    m_obj_col = Colour("454545");
    const string objDir = getBasePath() + "resources/obj/";
    const string objFile = objDir + "cube.obj";
	m_model_original = vvr::Mesh(objFile);
    reset();

}

void Mesh3DScene::reset()
{
    Scene::reset();
	std::cout << "JOKO" << endl;

    //! Define what will be vissible by default
    m_style_flag = 0;
    m_style_flag |= FLAG_SHOW_SOLID;
    m_style_flag |= FLAG_SHOW_WIRE;
    //m_style_flag |= FLAG_SHOW_PLANE;

	m_model = m_model_original;
}

void Mesh3DScene::resize()
{
    //! By Making `first_pass` static and initializing it to true,
    //! we make sure that the if block will be executed only once.

    static bool first_pass = true;

    if (first_pass)
    {
        m_model_original.setBigSize(getSceneWidth() / 2);
        m_model_original.update();
        m_model = m_model_original;
        Inits();
        first_pass = false;
    }
}


void Mesh3DScene::arrowEvent(ArrowDir dir, int modif)
{
    /*math::vec n = vertex_sep_tool->plane.normal;
	if (dir == UP) vertex_sep_tool->plane.d += 1;
    if (dir == DOWN)  -= 1;
    else if (dir == LEFT) n = math::float3x3::RotateY(DegToRad(1)).Transform(n);
    else if (dir == RIGHT) n = math::float3x3::RotateY(DegToRad(-1)).Transform(n);
    m_plane = Plane(n.Normalized(), m_plane_d);*/
}

void Mesh3DScene::keyEvent(unsigned char key, bool up, int modif)
{
    Scene::keyEvent(key, up, modif);
    key = tolower(key);
	int ctrl = modif & 0x01;
	int shift = modif & 0x02;
	
    switch (key)
    {
		case 's': m_style_flag ^= FLAG_SHOW_SOLID; break;
		case 'w': m_style_flag ^= FLAG_SHOW_WIRE; break;
		case 'n': m_style_flag ^= FLAG_SHOW_NORMALS; break;
		case 'a': m_style_flag ^= FLAG_SHOW_AXES; break;
		case 'p': m_style_flag ^= FLAG_VERTEX_SEPERATOR;
			      if(m_style_flag & FLAG_VERTEX_SEPERATOR){
					vertex_sep_tool = new Vertex_seperator_of_mesh_region(adjacency_of_tri);
					std::vector<int> index;
					adjacency_of_tri->get_indices_of_adjac_tris(index);
					m_canvas.clear();
					vertex_sep_tool->find_plane_that_seperates_verts_in_the_middle(index, m_model, m_canvas);}
				  if(!(m_style_flag & FLAG_VERTEX_SEPERATOR))
					  m_canvas.clear();
				  break;
		case 'b': m_style_flag ^= FLAG_SHOW_AABB; break;
		case 't': m_style_flag ^= FLAG_RAY_CAST_PICKING;
				  break;
		case 'd': m_style_flag ^= FLAG_SHOW_DUAL_GRAPH; 
				  if(m_style_flag & FLAG_SHOW_DUAL_GRAPH)
				  {
					dual_graph = new Dual_Graph();
					std::vector<vvr::Triangle> tris = m_model.getTriangles();
					dual_graph->find_dual_graph(tris);
				  }
				
				  break;
		case 'j': m_style_flag ^= FLAG_SHOW_ADJACENT_TRIS;
				  if(m_style_flag & FLAG_SHOW_ADJACENT_TRIS)
				  {
					dual_graph = new Dual_Graph();
					std::vector<vvr::Triangle> tris = m_model.getTriangles();
					dual_graph->find_dual_graph(tris);
				  }
			      break;
		case 'v': m_style_flag ^= FLAG_SUBDIVIDE_REGION_OF_MESH;
				  if(m_style_flag & FLAG_SUBDIVIDE_REGION_OF_MESH)
				  {
					subdivision_tool = new Processing_of_triangles_tool(adjacency_of_tri, adjac_of_tri_region, m_model);
					i = 0;
					subdivision_tool->subdivide_region_of_tris_of_mesh(m_model, i, i, dual_graph, adjac_of_tri_region, adjacency_of_tri);
					
				  }
				  break;
		case 'r': reset();
				  break;

	}
}


void Mesh3DScene::mousePressed(int x, int y, int modif)
{
	
    m_x = x;
    m_y = y;
	cout << "m_x = " << m_x << "\n";
	cout << "m_y = " << m_y << "\n";
	if(modif)
	{
		//Scene::mousePressed(x, y, modif);
	
		if(m_style_flag & FLAG_RAY_CAST_PICKING)
	{
		Ray ray;
		find_ray(ray, m_x, m_y);
		
		ray_cast_picking_tool = new Ray_cast_picking_tool(ray);
		ray_cast_pick_instance_exists = true;

		math::vec inters_pt;
		ray_cast_picking_tool->ray_cast_picking(m_model, inters_pt);
	}
	else if(ray_cast_pick_instance_exists)
	{
		ray_cast_pick_instance_exists = false;
		//delete ray_cast_picking_tool;
	}

	 if(m_style_flag & FLAG_SHOW_ADJACENT_TRIS)
	 {
		
		//Step 1 : ray cast picking
		Ray ray;
		find_ray(ray, m_x, m_y);
		//index_of_tris_inters_with_ray.clear();
		ray_cast_picking_tool = new Ray_cast_picking_tool(ray);
		ray_cast_pick_instance_exists = true;
		math::vec inters_pt;
		
		adjacency_instance_exists = ray_cast_picking_tool->ray_cast_picking(m_model, inters_pt);
		//Step 2 : find adjacent tris of picked tri
		if(adjacency_instance_exists)
		{
			std::vector<int> index(0);
			ray_cast_picking_tool ->get_index(index);
			adjacency_of_tri = new Adjacency_of_triangle(index[0]);
			adjacency_of_tri->find_indices_of_adj_tris_of_grade_n(dual_graph);
			///
			adjac_of_tri_region = new Adjacency_of_tri_region(adjacency_of_tri);
			adjac_of_tri_region->handle_adjacency_of_boundary_tris(m_model, dual_graph);
		}
	 }else if(adjacency_instance_exists)
	 {
		 ray_cast_pick_instance_exists = false;
		 adjacency_instance_exists = false;
        //delete ray_cast_picking_tool;
	 }	

	
	}
	else
		Scene::mousePressed(x, y, modif);

	
}

void Mesh3DScene::mouseMoved(int x, int y, int modif)
{
	Scene::mouseMoved(x, y, modif);
	m_x = x;
	m_y = y;
	cout << "m_x = " << m_x << std::endl;
	cout << "m_y = " << m_y << std::endl;
}

void Mesh3DScene::mouseReleased(int x, int y, int modif)
{
   m_x = x;
   m_y = y;
   cout << "m_x = " << m_x << "\n";
   cout << "m_y = " << m_y << "\n";
}


void Mesh3DScene::Inits()
{
	//initializations for attributes of the obj
    m_center_mass = vec(0, 0, 0);
    findCenterMass(m_model.getVertices(), m_center_mass);
    alignOriginTo(m_model.getVertices(), pick_Origin());
    findCenterMass(m_model.getVertices(), m_center_mass);
    findAABB(m_model.getVertices(), m_aabb);

	//erwt 2b
	adjacency_instance_exists = false;
	ray_cast_pick_instance_exists = false;
	
}


void Mesh3DScene::draw()
{
    //! Draw plane
	if (m_style_flag & FLAG_VERTEX_SEPERATOR) 
	{
		vertex_sep_tool->draw(m_model);
		vertex_sep_tool->draw_region(m_model);
    }
	
    if (m_style_flag & FLAG_SHOW_SOLID) m_model.draw(m_obj_col, SOLID);
    if (m_style_flag & FLAG_SHOW_WIRE) m_model.draw(Colour::black, WIRE);
    if (m_style_flag & FLAG_SHOW_NORMALS) m_model.draw(Colour::black, NORMALS);
    if (m_style_flag & FLAG_SHOW_AXES) m_model.draw(Colour::black, AXES);

    //! Draw center mass
    Point3D(m_center_mass.x, m_center_mass.y, m_center_mass.z, Colour::red).draw();

    //! Draw AABB
    if (m_style_flag & FLAG_SHOW_AABB) 
	{
        m_aabb.setColour(Colour::black);
        m_aabb.setTransparency(1);
        m_aabb.draw();
    }
	
	//! Draw line segments of dual graph
	if(m_style_flag & FLAG_SHOW_DUAL_GRAPH)
		dual_graph->draw();
			
	//! Draw triangle picked by rayCastPincking
	if((m_style_flag & FLAG_RAY_CAST_PICKING) && ray_cast_pick_instance_exists)
		ray_cast_picking_tool->draw(m_model);

	//! Draw adjacent tris(2b)
	if((m_style_flag & FLAG_SHOW_ADJACENT_TRIS) && adjacency_instance_exists)
	{
		adjacency_of_tri->draw_adj_tris(m_model);
		adjac_of_tri_region->draw(m_model);
	}
	//! Draw subdivided tris(3a)
	if((m_style_flag & FLAG_SUBDIVIDE_REGION_OF_MESH))
		
		subdivision_tool->draw(m_model);
		
	
	//! Draw m_canvas
	m_canvas.draw();



}



int main(int argc, char* argv[])
{
    //try {
        return vvr::mainLoop(argc, argv, new Mesh3DScene);
    //}
    /*catch (std::string exc) {
        cerr << exc << endl;
        return 1;
    }
    catch (...)
    {
        cerr << "Unknown exception" << endl;
        return 1;
    }*/
}

//! Apallaktikh Tasks

void findCenterMass(vector<vec> &vertices, vec &cm)
{
	vec temp_vec(0, 0, 0);
	for(unsigned int i = 0 ; i < vertices.size(); i++)
		temp_vec += vertices[i];
	cm = temp_vec/vertices.size();
}

void findAABB(vector<vec> &vertices, Box3D &aabb)
{
  
	float x_min = vertices[0].x, x_max = vertices[0].x;
	float y_min = vertices[0].y, y_max = vertices[0].y;
	float z_min = vertices[0].z, z_max = vertices[0].z;

	for(unsigned int i = 1 ; i < vertices.size(); i++)
	{
		if(vertices[i].x > x_max)
			x_max = vertices[i].x;
		if(vertices[i].y > y_max)
			y_max = vertices[i].y;
		if(vertices[i].z > z_max)
			z_max = vertices[i].z;
		if(vertices[i].x < x_min )
			x_min = vertices[i].x;
		if(vertices[i].y < y_min )
			y_min = vertices[i].y;
		if(vertices[i].z < z_min )
			z_min = vertices[i].z;
	}

	aabb.x1 = x_min;
	aabb.x2 = x_max;
	aabb.y1 = y_min;
	aabb.y2 = y_max;
	aabb.z1 = z_min;
	aabb.z2 = z_max;
}

vec Mesh3DScene::pick_Origin()
{
    return m_center_mass;
}

void alignOriginTo(vector<vec> &vertices, const vec &cm)
{

	for(unsigned int i = 0 ; i < vertices.size(); i++)
		vertices[i] -= cm;
}




////////////////////////////function_for_implementation_of_question_2a/////////////////////////////////////////////////////

void Mesh3DScene::find_ray(Ray &ray, int m_x, int m_y)
{
	//Step1 : given the window coords, define the ray that:
	//has beging_pt the object coords of camera's position
	//direction that is defined by begin_pt and object coords that correspond to m_x,m_y windows coords
	//--> intersection pts of ray with near and far clipping planes
	ray = unproject(m_x, m_y);
}

