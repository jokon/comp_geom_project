/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/

#ifndef _RAY_CAST_PICKER
#define _RAY_CAST_PICKER


#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
//#include "symmetriceigensolver3x3.h"
#include <VVRScene/scene.h>
#include <stdlib.h>


//kanonika to raycastpicking tha eprepe na exei ws fields mono to Ray ray kai ena Object object_picked pou na mporei na einai otidhpote
//sthn sugkekrimenh periptwsh thewrw oti mporei na exei intersect me trigwna mono tou mesh kai epilegw auto pou exei thn mikroterh apostash 
//apo thn kamera
class Ray_cast_picking_tool
{
	//private fields
	math::Ray ray;
	std::vector<int> mesh_index_of_picked_tri;

private:
	
	void min(std::vector<float> distances, int &min, int &index_of_min);
	void find_intersections_of_ray_with_mesh_tris(std::vector<vvr::Triangle> triangles, std::vector<int> &indices_of_intersected_by_tris, std::vector<float> &distances_of_inters_pts_from_camera);
	bool ray_intersects_AABB_of_mesh3d(math::AABB aabb);
	bool ray_intersects_triangle(vvr::Triangle tri, std::vector<float> &distances_of_inters_pts_from_camera);
public:
	Ray_cast_picking_tool(math::Ray ray);
	bool get_index(std::vector<int> &index);
	bool ray_cast_picking(vvr::Mesh mesh, math::vec &intersection_pt);
	void draw(vvr::Mesh mesh);

};


Ray_cast_picking_tool::Ray_cast_picking_tool(math::Ray ray)
{
	//initializations of fields
	//ray
	this->ray = ray;
	//mesh_index_of_picked_tri
	mesh_index_of_picked_tri.clear();
}

bool Ray_cast_picking_tool::get_index(std::vector<int> &index)
{
	if(mesh_index_of_picked_tri.empty())
		return false;
	else
	{
		index = mesh_index_of_picked_tri;
		return true;
	}

}

bool Ray_cast_picking_tool::ray_intersects_AABB_of_mesh3d(math::AABB aabb)
{
	
	//Step 1 : check for intersection and regarding the result return the proper bool value
	if(ray.Intersects(aabb))
		return true;
	else
		return false;

}

bool Ray_cast_picking_tool::ray_intersects_triangle( vvr::Triangle tri, std::vector<float> &distances_of_inters_pts_from_camera)
{
	//Step1 : convert triangle from vvr::Triangle to math::Triangle
	const vec v1 = tri.v1();
	const vec v2 = tri.v2();
	const vec v3 = tri.v3();

	math::Triangle triangle(v1, v2, v3);

	//Step2 : check for intersection of ray with triangle
	//regarding the result return appropriate bool value
	//if intersection is true, then :
	//save the distance of intersection pt from cam_position obj coords
	vec inters_pt;
	float dist = 0;
	if(ray.Intersects(triangle, &dist, &inters_pt ))
	{
		distances_of_inters_pts_from_camera.push_back(dist);
		return true;
	}
	else
		return false;
}


void Ray_cast_picking_tool::find_intersections_of_ray_with_mesh_tris(std::vector<vvr::Triangle> triangles, std::vector<int> &indices_of_intersected_tris, std::vector<float> &distances_of_inters_pts_from_camera)
{

	//Step1 : for every triangle check if it intersects with ray
	for(unsigned int i = 0 ; i < triangles.size() ; i++)
	{
		//Step2 : if ray intersects tri, save to indices_of_tris_intersected_by_ray, tri's index from triangles vector
		//indices_of_tris_intersected_by_ray vector helps to know to which 
		//triangle corresponds each distance of distances_of_inters_pts_from_camera
		if(ray_intersects_triangle(triangles[i], distances_of_inters_pts_from_camera))
			indices_of_intersected_tris.push_back(i);
	}

}

void Ray_cast_picking_tool::min(std::vector<float> distances, int &min,int &index_of_min)
{
	for(unsigned int i = 1 ; i < distances.size() ; i++)
	{
		if(min > distances[i])
		{
			min = distances[i];
			index_of_min = i;
		}	
	}
}

bool Ray_cast_picking_tool::ray_cast_picking(vvr::Mesh mesh, math::vec &intersection_pt)
{
	mesh_index_of_picked_tri.clear();
	//Step1 : check for intersection of ray with AABB of mesh
	if(ray_intersects_AABB_of_mesh3d( mesh.getAABB()))
	{
		std::cout << "\n" << "Ray intersects with AABB of mesh3d." << "\n";

		std::vector<int> indices_of_intersected_tris(0);
		std::vector<float> distances_of_inters_pts_from_camera(0);
		std::vector<vvr::Triangle> triangles = mesh.getTriangles();

		//Step2 : there is intersection of ray with aabb,so : -->
		//--> check for intersection of ray with the triangles of the mesh3d
		find_intersections_of_ray_with_mesh_tris(triangles, indices_of_intersected_tris, distances_of_inters_pts_from_camera);
	
		//Step3 : find the intersection point with the minimum distance
		//from the coords of camera's position
		if(!indices_of_intersected_tris.empty())
		{
			std::cout << "ray intersects with mesh" << "\n";
			int Min = distances_of_inters_pts_from_camera[0], index_of_min = 0;
			min(distances_of_inters_pts_from_camera, Min, index_of_min);
			math::vec pt = triangles[indices_of_intersected_tris[index_of_min]].getCenter();
			 
			intersection_pt = pt;
			mesh_index_of_picked_tri.push_back(indices_of_intersected_tris[index_of_min]);	
			return true;
		}
		else 
		{
			std::cout << " No intersection with mesh's triangles." <<"\n";
			return false;}
	}
	else
	{
		std::cout << "\n" << "Ray does not intersect with AABB of mesh3d." << "\n";
		return false;
	}
}


void Ray_cast_picking_tool::draw(vvr::Mesh mesh)
{
	std::vector<int> index = mesh_index_of_picked_tri;
	std::vector<vvr::Triangle> tris = mesh.getTriangles();
	for(unsigned int i = 0; i < index.size() ; i++)
	{
		vvr::Triangle3D picked_tri( tris[index[i]].v1().x, tris[index[i]].v1().y, tris[index[i]].v1().z, tris[index[i]].v2().x, tris[index[i]].v2().y, tris[index[i]].v2().z, tris[index[i]].v3().x, tris[index[i]].v3().y, tris[index[i]].v3().z, vvr::Colour::darkOrange);
		picked_tri.draw();
	}

}


#endif
