/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/
#ifndef _VERTEX_SEPERATOR
#define _VERTEX_SEPERATOR


#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
//#include "symmetriceigensolver3x3.h"
#include <VVRScene/scene.h>
#include <stdlib.h>
#include "adjacencyOfGradeN.h"

using namespace std;
using namespace vvr;

class Vertex_seperator_of_mesh_region 
{
	
	public std::vector<int> mesh_inds_of_tris_region;
	public math::vec mean_normal;
public:
	math::Plane plane;

	
private:
	void find_mean_normal_of_mesh_region(vector<int> indices_of_region_tris_of_mesh, Mesh mesh);
	void find_vertices_of_tris_that_are_in_the_selected_region(std::vector<int> indices_of_tris, vvr::Mesh mesh, std::vector<math::vec> &vertices_found);
	bool vert_already_exists_in_vector(math::vec vert, std::vector<math::vec> verts_vector);
	void find_distance(std::vector<math::vec> verts_in_the_region, math::Plane pl, std::vector<float> &distance_vector);
public:
	Vertex_seperator_of_mesh_region(Adjacency_of_triangle * adj_of_tri);
	void find_plane_that_seperates_verts_in_the_middle(std::vector<int> indices_of_tris, vvr::Mesh mesh, vvr::Canvas2D &m_canvas);
	void draw(vvr::Mesh mesh);
	////
	void draw_region(vvr::Mesh mesh);
};


public Vertex_seperator_of_mesh_region(Adjacency_of_triangle * adj_of_tri)
{
	//initializations
	//mean_normal
	Vertex_seperator_of_mesh_region::mean_normal.x = 0;
	Vertex_seperator_of_mesh_region::mean_normal.y = 0;
	Vertex_seperator_of_mesh_region::mean_normal.z = 1;

	//plane
	plane = Plane(Vertex_seperator_of_mesh_region::mean_normal,0);

	//mesh_inds_of_tris_region
	adj_of_tri -> get_indices_of_adjac_tris(mesh_inds_of_tris_region);
	
}

void find_mean_normal_of_mesh_region(vector<int> inds_of_tris_of_region, Mesh mesh)
{ 
	math::vec normal(0,0,0);
	vector<vvr::Triangle> tris = mesh.getTriangles();
	for(unsigned int i = 0 ; i < inds_of_tris_of_region.size() ; i++)
	{
		//Step 1 : for each triangle of mesh region find its normal
		//add it to normal vec
		vvr::Triangle tri = tris[inds_of_tris_of_region[i]];
		normal += tri.getNormal(); 
	}

	//Step 2 : normalize the normal vec 
	float norm_length = std::sqrt(std::pow(normal.x,2) + std::pow(normal.y,2) + std::pow(normal.z, 2));
	normal /= norm_length;

	//Step 3 : update mean_normal field
	Vertex_seperator_of_mesh_region::mean_normal = normal;
}



bool vert_already_exists_in_vector(math::vec vert, std::vector<math::vec> verts_vector)
{
	//Step 1 : set an 'exists' flag = false
	bool exists = false;
	for(unsigned int k = 0 ; k < verts_vector.size() ; k++)
	{
		if(verts_vector[k].Equals(vert))
		{
			exists = true;
			break;
		}
	}

	//Step 2 : if exists = false, then return false
	//if not do the opposite
	if(exists)
		return true;
	else 
		return false;

}

void find_vertices_of_tris_that_are_in_the_selected_region(std::vector<int> indices_of_tris, vvr::Mesh mesh, std::vector<math::vec> &vertices_found)
{
	for(unsigned int i = 0 ; i < indices_of_tris.size() ; i++)
	{
		//Step 1 : find vertices of mesh[indices_of_tris[i]]
		vvr::Triangle tri = mesh.getTriangles()[indices_of_tris[i]];
		std::vector<math::vec> verts(0);
		verts.push_back(tri.v1());
		verts.push_back(tri.v2());
		verts.push_back(tri.v3());

		//Step 2 : check if verts of tri already exist in vertices_vector
		for(unsigned int j = 0 ; j < verts.size() ; j++)
		{
			//if not then save vertex to vertices_vector
			if(!vert_already_exists_in_vector(verts[j], vertices_found))
				vertices_found.push_back(verts[j]);
		}
	}
}

void find_distance(std::vector<math::vec> verts_in_the_region, math::Plane pl, std::vector<float> &dist_vector)
{
	for(unsigned int k = 0 ; k < verts_in_the_region.size() ; k++)
		 dist_vector.push_back(pl.SignedDistance(verts_in_the_region[k]));
}

void find_plane_that_seperates_verts_in_the_middle(std::vector<int> indices_of_tris, vvr::Mesh mesh, vvr::Canvas2D &m_canvas)
{
	//Step 1 : 
	find_mean_normal_of_mesh_region(indices_of_tris,mesh);
	//Step 2 : set the normal of plane equal to mean_normal and the offset of origin to zero
	vec v(0, 0, 0);
	plane.Set(v,mean_normal);

	//Step 2 : find the vertices of tris that are in the selected region 
	std::vector<math::vec> verts_of_selected_tri_region(0);
	find_vertices_of_tris_that_are_in_the_selected_region(indices_of_tris, mesh, verts_of_selected_tri_region);
	
	//Step 3 : add vertices found in the region selsected to m_canvas to draw them afterwards
	for(unsigned int i = 0 ; i < verts_of_selected_tri_region.size() ; i++)
		m_canvas.add(new Point3D(verts_of_selected_tri_region[i].x,verts_of_selected_tri_region[i].y, verts_of_selected_tri_region[i].z,vvr::Colour::magenta));

	//Step 4 : find plane's distance from each vertex
	std::vector<float> distance_vector;
	distance_vector.clear();
	find_distance(verts_of_selected_tri_region, plane, distance_vector);

	//Step 5 : sort distance_vector
	std::sort(distance_vector.begin(),distance_vector.end());

	//Step 6 : check if the distance_vector.size() is odd or even
	int size = distance_vector.size();
	float d;
	if(distance_vector.size()%2)//odd
	{
		 
		d = distance_vector[int(size/2.0 + 0.5 - 1)];
		plane.Translate(-mean_normal * d);
	}
	else
	{
		d = (distance_vector[size/2 - 1] + distance_vector[size/2 ])/2;
		plane.Translate(-mean_normal * d);
	
	}

	//Step 7 : check that the number of verts found in the 'positive' and in the 'negative' side of the plane are both equal
	int posit_count = 0;
	int neg_count = 0;
	for(unsigned int i = 0 ; i < verts_of_selected_tri_region.size() ; i++)
	{
		
		if(plane.IsOnPositiveSide(verts_of_selected_tri_region[i]))
			posit_count++;
		else
			neg_count++;
	}

	std::cout << "posit_count" << posit_count <<std::endl;
	std::cout << "neg_count" << neg_count << std::endl;
}


void draw_region(vvr::Mesh mesh)
{
	//! Draw tris of selected region
	std::vector<vvr::Triangle> tris = mesh.getTriangles();
	for(unsigned int i = 0; i < mesh_inds_of_tris_region.size() ; i++)
	{
		vvr::Triangle3D picked_tri( tris[mesh_inds_of_tris_region[i]].v1().x, tris[mesh_inds_of_tris_region[i]].v1().y, tris[mesh_inds_of_tris_region[i]].v1().z, tris[mesh_inds_of_tris_region[i]].v2().x, tris[mesh_inds_of_tris_region[i]].v2().y, tris[mesh_inds_of_tris_region[i]].v2().z, tris[mesh_inds_of_tris_region[i]].v3().x, tris[mesh_inds_of_tris_region[i]].v3().y, tris[mesh_inds_of_tris_region[i]].v3().z, vvr::Colour::darkOrange);
		picked_tri.draw();
	}
}

void draw(vvr::Mesh mesh)
{
	//! Draw tris of selected region
	std::vector<vvr::Triangle> tris = mesh.getTriangles();
	for(unsigned int i = 0; i < mesh_inds_of_tris_region.size() ; i++)
	{
		vvr::Triangle3D picked_tri( tris[mesh_inds_of_tris_region[i]].v1().x, tris[mesh_inds_of_tris_region[i]].v1().y, tris[mesh_inds_of_tris_region[i]].v1().z, tris[mesh_inds_of_tris_region[i]].v2().x, tris[mesh_inds_of_tris_region[i]].v2().y, tris[mesh_inds_of_tris_region[i]].v2().z, tris[mesh_inds_of_tris_region[i]].v3().x, tris[mesh_inds_of_tris_region[i]].v3().y, tris[mesh_inds_of_tris_region[i]].v3().z, vvr::Colour::darkOrange);
		picked_tri.draw();
	}
	//! Draww plane
	 vvr::Colour colPlane(0x41, 0x14, 0xB3);
     float u = 40, v = 40;
     math::vec p0(plane.Point(-u, -v, math::vec(0, 0, 0)));
     math::vec p1(plane.Point(-u, v, math::vec(0, 0, 0)));
     math::vec p2(plane.Point(u, -v, math::vec(0, 0, 0)));
     math::vec p3(plane.Point(u, v, math::vec(0, 0, 0)));
     math2vvr(math::Triangle(p0, p1, p2), colPlane).draw();
     math2vvr(math::Triangle(p2, p1, p3), colPlane).draw();

}


#endif
