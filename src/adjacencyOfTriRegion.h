/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/
#ifndef _ADJACENCY_OF_TRI_REGION
#define _ADJACENCY_OF_TRI_REGION

#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
//#include "symmetriceigensolver3x3.h"
#include <VVRScene/scene.h>
#include <stdlib.h>
#include "dualGraphOfMesh.h"
#include "adjacencyOfGradeN.h"

struct Bound_tris_adj_1
{
	//these are the outer boundary region triangles that are adjacent to one inner boundary triangle of region
	//by making this struct i organise my data so as to make the subdivision of outer boundary triangles of type 1 in "subdivisionOfRegionOfTriangles.h"

	//fields
	int common_vi1;
	int common_vi2;
	int adj_to_com_seg;
};

struct Bound_tris_adj_2
{
	
	//these are the outer boundary region triangles that are adjacent to two inner boundary triangle of region
	//by making this struct i organise my data so as to make the subdivision of outer boundary triangles of type 2 in "subdivisionOfRegionOfTriangles.h"
	int common_vi1;
	int common_vi2;
	int common_vi3;
	int common_vi4;
	int adj_to_com_seg_arbitrary;
};

class Adjacency_of_tri_region
{
	//private fields
	//std::vector<int> mesh_indices_of_regional_triangles;
	std::vector<int> mesh_indices_of_inner_boundary_tris;
	std::vector<int> mesh_indices_of_outer_boundary_tris;
	std::vector<int> mesh_indices_of_adj_tris_of_outer_boundary_tris;
	std::vector<struct Bound_tris_adj_1> type1_of_bound_region_tris;
	std::vector<struct Bound_tris_adj_2> type2_of_bound_region_tris;
	std::vector<int> mesh_indices_to_bound_tris_adj_1_vector;
	std::vector<int> mesh_indices_to_bound_tris_adj_2_vector;
	std::vector<int> num_of_adj_inner_tris_for_each_outer_bound_tri;

public:
	std::vector<int> mesh_indices_of_regional_triangles;//make it private again
private:
	void find_indices_of_adjacent_tris(std::vector<int> &inds_of_adj_tr, int ind_of_tri, Dual_Graph * dual_graph);
	void find_inner_boundary_triangles(Dual_Graph * dual_graph);
	void find_outer_boundary_triangles(Dual_Graph * dual_graph);
	void find_inds_of_adj_tris_of_outer_bound_tris(Dual_Graph * dual_graph);
	bool ind_already_exists_to_ind_vector(int ind, std::vector<int> ind_vector);
	void find_common_inds_of_verts_of_2_tris(int index1, int index2, vvr::Mesh mesh, std::vector<int> &inds_of_common_verts_of_tris);
	void find_ind_of_vert_adjac_to_segment(int vi1, int vi2, int mesh_ind_of_outer_tri, vvr::Mesh mesh, std::vector<int> &ret);
	void find_common_verts_of_boundary_tris(vvr::Mesh mesh, std::vector<int> &inds_of_common_verts_of_bound_tris);
	void find_inds_of_verts_adjac_to_common_segments(vvr::Mesh mesh, std::vector<int> inds_of_common_verts_of_bound_tris, std::vector<int> &adj_to_seg_verts_inds);
	void find_common_vert_of_2_inner_tris_adj_to_the_same_outer_tri(vvr::Mesh mesh, int ind1, int ind2, math::vec &com_pt);
	void find_data_about_bound_tris_adjacency(vvr::Mesh mesh, std::vector<int> &inds_of_common_verts_of_bound_tris, std::vector<int> &adj_to_seg_verts_inds);
	void organize_data_of_adjacency_of_outer_boundary_tris(std::vector<int> inds_of_common_verts_of_bound_tris, std::vector<int> inds_of_verts_adj_to_com_segm);
public:
	void set_mesh_inds_vector_of_regional_tris(std::vector<int> inds_of_region_tris_to_be_processed);
	bool get_inner_boundary_tris(std::vector<int> &inner_vector);
	bool get_outer_boundary_tris(std::vector<int> &outer_vector);
	bool get_type_1_tris_info_vectors(std::vector<Bound_tris_adj_1> &type1, std::vector<int> &mesh_indices_of_boundary_type_1);
	bool get_type_2_tris_info_vectors(std::vector<Bound_tris_adj_2> &type2, std::vector<int> &mesh_indices_of_boundary_type_2);
	bool get_type_3_tris_indices(std::vector<int> &mesh_indices_of_boundary_type3);
	void handle_adjacency_of_boundary_tris(vvr::Mesh mesh, Dual_Graph * dual_graph);
	Adjacency_of_tri_region(Adjacency_of_triangle *adjacency_of_tri);

	
	void draw(vvr::Mesh mesh);
};



Adjacency_of_tri_region::Adjacency_of_tri_region(Adjacency_of_triangle * adjacency_of_tri)
{
	//initializations
	
	//mesh_indices_of_regional_triangles
	adjacency_of_tri -> get_indices_of_adjac_tris(mesh_indices_of_regional_triangles);
	
	//mesh_indices_of_inner_boundary_tris
	mesh_indices_of_inner_boundary_tris.clear();

	//mesh_indices_of_outer_boundary_tris
	mesh_indices_of_outer_boundary_tris.clear();

	//mesh_indices_of_adj_tris_of_outer_boundary_tris
	mesh_indices_of_adj_tris_of_outer_boundary_tris.clear();

	//type1_of_bound_region_tris
	type1_of_bound_region_tris.clear();
	
	//type2_of_bound_region_tris
	type2_of_bound_region_tris.clear();

	//num_of_adj_inner_tris_for_each_outer_bound_tri
	num_of_adj_inner_tris_for_each_outer_bound_tri.clear();

	//mesh_indices_to_bound_tris_adj_1_vector;
	mesh_indices_to_bound_tris_adj_1_vector.clear();

	//mesh_indices_to_bound_tris_adj_2_vector;
	mesh_indices_to_bound_tris_adj_2_vector.clear();

}

void Adjacency_of_tri_region::find_indices_of_adjacent_tris(std::vector<int> &inds_of_adj_tr, int ind_of_tri, Dual_Graph *dual_graph)
{

	//Step 1 : get adjacency vector of dual_graph
	std::vector<int> adjacency_vector(0);
	dual_graph->get_indices_of_adjacent_tris(adjacency_vector);

	//Step 2 : find indices of adjacent tris of tri with index ind_of_tri
	inds_of_adj_tr.push_back(adjacency_vector[3 * ind_of_tri]);
	inds_of_adj_tr.push_back(adjacency_vector[3 * ind_of_tri + 1]);
	inds_of_adj_tr.push_back(adjacency_vector[3 * ind_of_tri + 2]);
}


bool Adjacency_of_tri_region::ind_already_exists_to_ind_vector(int ind, std::vector<int> ind_vector)
{
	bool exists = false;
	
	for(unsigned int k = 0 ; k < ind_vector.size() ; k++)
	{
		if( ind == ind_vector[k] )
		{
			exists = true;
			break;
		}
	}

	if(exists)
		return true;
	else
		return false;
}


void Adjacency_of_tri_region::find_inner_boundary_triangles(Dual_Graph *dual_graph)
{
	mesh_indices_of_inner_boundary_tris.clear();
	//make a copy of indices_to_tris_of_mesh_to_be_processed
	std::vector<int> inds_to_tris_to_be_processed = mesh_indices_of_regional_triangles;
	//for every tri of indices_to_tris_of_mesh_to_be_processed check if it is inner boundary triangle
	for(unsigned int w = 0 ; w < mesh_indices_of_regional_triangles.size() ; w++)
	{

		//Step 1 : find the index of indices_to_tris_of_mesh_to_be_processed[w] to inds_to_tris_to_be_processed vector
		int index = -1;
		for(unsigned int x = 0 ; x < inds_to_tris_to_be_processed.size() ; x++)
		{
			if( mesh_indices_of_regional_triangles[w] == inds_to_tris_to_be_processed[x])
			{
				index = x;
				break;
			}
		}
	
		//Step 2 : find the adjacent tris of the triangle
		std::vector<int> inds_of_adj_tr(0);
		find_indices_of_adjacent_tris(inds_of_adj_tr, mesh_indices_of_regional_triangles[w], dual_graph);
		
		//Step 3 : if all three inds_of_adj_tr exist in indices_to_tris_of_mesh_to_be_processed,
		//erase current ind  from vector 
		if(ind_already_exists_to_ind_vector(inds_of_adj_tr[0], mesh_indices_of_regional_triangles) && ind_already_exists_to_ind_vector(inds_of_adj_tr[1], mesh_indices_of_regional_triangles) && ind_already_exists_to_ind_vector(inds_of_adj_tr[2], mesh_indices_of_regional_triangles))
		{
			inds_to_tris_to_be_processed.erase(inds_to_tris_to_be_processed.begin() + index);
			
		}


	}

	//Step 4 : push back to inner_boundary_tris_selected the inds of inner boundary tris found
	std::vector<int> inner_boundary_tris_selected(0);
	for(unsigned int w = 0 ; w < inds_to_tris_to_be_processed.size() ; w++)
		inner_boundary_tris_selected.push_back(inds_to_tris_to_be_processed[w]);

	mesh_indices_of_inner_boundary_tris = inner_boundary_tris_selected;

}


void Adjacency_of_tri_region::find_outer_boundary_triangles(Dual_Graph * dual_graph)
{
	mesh_indices_of_outer_boundary_tris.clear();
	//for each tri of selected region
	for(unsigned int i = 0 ; i < mesh_indices_of_regional_triangles.size() ; i++)
	{
		//Step 1 : find its adjacent_tris indices
		std::vector<int> inds_of_adj_tr(0);
		find_indices_of_adjacent_tris(inds_of_adj_tr, mesh_indices_of_regional_triangles[i], dual_graph);
		
		//Step 2 : if any of the indices of adjacent tris does not exist to mesh_indices_of_regional_triangles:
		for(unsigned int j = 0 ; j < 3 ; j++)
		{
			if(!ind_already_exists_to_ind_vector(inds_of_adj_tr[j], mesh_indices_of_regional_triangles) && inds_of_adj_tr[j] != -1) 
			{
				//Step 3:if it does not already exists to mesh_indices_of_outer_triangles, push it back to that vector
				if(!ind_already_exists_to_ind_vector(inds_of_adj_tr[j], mesh_indices_of_outer_boundary_tris))
					mesh_indices_of_outer_boundary_tris.push_back(inds_of_adj_tr[j]);
			}
		}	
	}
}

void Adjacency_of_tri_region::set_mesh_inds_vector_of_regional_tris(std::vector<int> inds_of_region_tris_to_be_processed)
{
	mesh_indices_of_regional_triangles = inds_of_region_tris_to_be_processed;
}

bool Adjacency_of_tri_region::get_inner_boundary_tris(std::vector<int> &inner_vector)
{
	if(mesh_indices_of_inner_boundary_tris.empty())
		return false;
	else
	{
		inner_vector = mesh_indices_of_inner_boundary_tris;
		return true;
	}
}
	
bool Adjacency_of_tri_region::get_outer_boundary_tris(std::vector<int> &outer_vector)
{
	if(mesh_indices_of_outer_boundary_tris.empty())
		return false;
	else
	{
		outer_vector = mesh_indices_of_inner_boundary_tris;
		return true;
	}
}

bool Adjacency_of_tri_region::get_type_3_tris_indices(std::vector<int> &mesh_indices_of_boundary_type3)
{
	//Step 1 : find the mesh_indices of tris that have three adjacent triangles
	std::vector<int> ind_temp(0);
	for(unsigned int i = 0 ; i < num_of_adj_inner_tris_for_each_outer_bound_tri.size() ; i++)
	{
		if(num_of_adj_inner_tris_for_each_outer_bound_tri[i] == 3)
			ind_temp.push_back(mesh_indices_of_outer_boundary_tris[i]);
	}

	
	

	if(ind_temp.empty())
		return false;
	else 
	{
		//Step 2 : copy vector found to mesh_indices_of_boundary_type3
		mesh_indices_of_boundary_type3 = ind_temp;
		return true;
	}
}


void Adjacency_of_tri_region::find_inds_of_adj_tris_of_outer_bound_tris(Dual_Graph * dual_graph)
{

	mesh_indices_of_adj_tris_of_outer_boundary_tris.clear();
	//step 1 : initialize the mesh_indices_of_adj_tris_of_outer_boundary_tris vector
	//i suppose that every tri of outer boundary will have at most 3 triangles of inner boundary adjacent 
	//so size of mesh_indices_of_adj_tris_of_outer_boundary_tris is going to be 3 * mesh_indices_of_mesh_indices_of_outer_boundary_tris
	//if 'i' outer bound tri has only one adjacent inner tri then, mesh_indices_of_adj_tris_of_outer_boundary_tris[3 * i + 1] = -1
	for(unsigned int i = 0 ; i < 3 * mesh_indices_of_outer_boundary_tris.size() ; i++ )
		mesh_indices_of_adj_tris_of_outer_boundary_tris.push_back(-1);

	//step 2 : push_back to a temp vector all the indices of adjacent tris
	//each triplet of values of temp vector will have the indices of 'i' triangle
	std::vector<int> temp(0);
	for(unsigned int i = 0 ; i < mesh_indices_of_inner_boundary_tris.size() ; i++)
	{
		std::vector<int> adjac_inds(0);
		find_indices_of_adjacent_tris(adjac_inds, mesh_indices_of_inner_boundary_tris[i], dual_graph);
		for(unsigned int j = 0 ; j < 3 ; j++)
			temp.push_back(adjac_inds[j]);
	}

	num_of_adj_inner_tris_for_each_outer_bound_tri.clear();
	for (unsigned int i = 0 ; i < mesh_indices_of_outer_boundary_tris.size() ; i++)
		num_of_adj_inner_tris_for_each_outer_bound_tri.push_back(0);

	//step 3 : for every tri of the outer boundaries , find the indices of its adjacent tris of inner boundaries
	for(unsigned int j = 0 ; j < mesh_indices_of_outer_boundary_tris.size() ; j++)
	{
		for(unsigned int i = 0 ; i < temp.size(); i +=3)
		{
			if((mesh_indices_of_outer_boundary_tris[j] == temp[i]) || (mesh_indices_of_outer_boundary_tris[j] == temp[i + 1]) || (mesh_indices_of_outer_boundary_tris[j] == temp[i + 2]))
			{	
				mesh_indices_of_adj_tris_of_outer_boundary_tris[3 * j + num_of_adj_inner_tris_for_each_outer_bound_tri[j]] = mesh_indices_of_inner_boundary_tris[i/3];
				num_of_adj_inner_tris_for_each_outer_bound_tri[j] += 1;
			}
		}
	}


	//erase afterwards
	for(unsigned int i = 0 ; i < mesh_indices_of_adj_tris_of_outer_boundary_tris.size() ; i+=3)
	{
		std::cout << mesh_indices_of_adj_tris_of_outer_boundary_tris[i] << "," << mesh_indices_of_adj_tris_of_outer_boundary_tris[i + 1] << std::endl;
	} //cntr k cntr u -->uncomment



}




void Adjacency_of_tri_region::find_common_inds_of_verts_of_2_tris(int index1, int index2, vvr::Mesh mesh, std::vector<int> &inds_of_common_verts_of_tris)
{
	//Step 1 : for each tri of the two find its vertices
	std::vector<vvr::Triangle> tris = mesh.getTriangles(); 
	std::vector<math::vec> verts1(0);
	std::vector<math::vec> verts2(0);

	verts1.push_back(tris[index1].v1());
	verts2.push_back(tris[index2].v1());

	verts1.push_back(tris[index1].v2());
	verts2.push_back(tris[index2].v2());

	verts1.push_back(tris[index1].v3());
	verts2.push_back(tris[index2].v3());

	std::vector<math::vec> v1 = verts1;
	std::vector<math::vec> v2 = verts2;
	//Step 2 : check which of verts of verts1, verts2 vectors are common
	//there are 3 cases : (indices of verts1[0], verts1[1]) or (indices of verts1[0], verts1[2]) or (indices of verts1[1], verts1[2]) are indices of common verts of tris

	//indices of verts1[0], verts1[1]
	if (((v1[0].Equals(v2[0])) || (v1[0].Equals(v2[1])) || (v1[0].Equals(v2[2]))) && ((v1[1].Equals(v2[0])) || (v1[1].Equals(v2[1])) || (v1[1].Equals(v2[2]))))
	
	{
	//common inds of verts : tris[index1].vi1(),tris[index1].vi2() 
		inds_of_common_verts_of_tris.push_back(tris[index1].vi1);
		inds_of_common_verts_of_tris.push_back(tris[index1].vi2);
	}
	
	//indices of verts1[0], verts1[2]
	if (((v1[0].Equals(v2[0])) || (v1[0].Equals(v2[1])) || (v1[0].Equals(v2[2]))) && ((v1[2].Equals(v2[0])) || (v1[2].Equals(v2[1])) || (v1[2].Equals(v2[2]))))
	{
	//common inds of verts1[0], verts1[2]
		inds_of_common_verts_of_tris.push_back(tris[index1].vi1);
		inds_of_common_verts_of_tris.push_back(tris[index1].vi3);
	}

	//indices of verts1[1], verts1[2]
	if (((v1[2].Equals(v2[0])) || (v1[2].Equals(v2[1])) || (v1[2].Equals(v2[2]))) && ((v1[1].Equals(v2[0])) || (v1[1].Equals(v2[1])) || (v1[1].Equals(v2[2]))))
	{
	//common inds of verts1[1], verts1[2]
		inds_of_common_verts_of_tris.push_back(tris[index1].vi2);
		inds_of_common_verts_of_tris.push_back(tris[index1].vi3);
	}
}

void Adjacency_of_tri_region::find_common_verts_of_boundary_tris(vvr::Mesh mesh, std::vector<int> &inds_of_common_verts_of_bound_tris)
{
	//for every outer boundary tri:
	for(unsigned int i = 0 ; i < mesh_indices_of_adj_tris_of_outer_boundary_tris.size() ; i+=3)
	{
		for(unsigned int j = 0 ; j < 3; j++)
		{
			//Step 1 : check that second adjacent tri exists
			if(mesh_indices_of_adj_tris_of_outer_boundary_tris[i+j] != -1)
			{
				//Step 2 : find inds of common verts of each boundary tri(index --> i /2) with its adjacent inner boundary tri
				std::vector<int> mesh_inds_of_common_verts_of_tris(0); 
				find_common_inds_of_verts_of_2_tris(mesh_indices_of_outer_boundary_tris[i / 3], mesh_indices_of_adj_tris_of_outer_boundary_tris[i + j], mesh, mesh_inds_of_common_verts_of_tris);
				
				//Step 3 : save inds of common verts
				inds_of_common_verts_of_bound_tris.push_back(mesh_inds_of_common_verts_of_tris[0]);
				inds_of_common_verts_of_bound_tris.push_back(mesh_inds_of_common_verts_of_tris[1]);

				
			}
			else
			{
				inds_of_common_verts_of_bound_tris.push_back(-1);
				inds_of_common_verts_of_bound_tris.push_back(-1);
			}
		}
	}


}


void Adjacency_of_tri_region::find_ind_of_vert_adjac_to_segment(int vi1, int vi2, int mesh_ind_of_outer_tri, vvr::Mesh mesh, std::vector<int> &ret)
{
	//Step 1 : find triangle's mesh indices of verts
	std::vector<vvr::Triangle> tris = mesh.getTriangles();
	std::vector<int> inds(0);

	inds.push_back(tris[mesh_ind_of_outer_tri].vi1);
	inds.push_back(tris[mesh_ind_of_outer_tri].vi2);
	inds.push_back(tris[mesh_ind_of_outer_tri].vi3);

	//Step 2 : check which index of inds is not equal to vi1 and vi2
	//the vertex with the index is adjacent to segment
	if((inds[0] != vi1) && (inds[0] != vi2) )
		ret.push_back(inds[0]);
	else if((inds[1] != vi1) && (inds[1] != vi2))
		ret.push_back(inds[1]);
	else if((inds[2] != vi1) && (inds[2] != vi2))//////i make it just tocheck everything is alright, afterwards erase last if///////////////////////////////////////////////////////
		ret.push_back(inds[2]);
}

void Adjacency_of_tri_region::find_common_vert_of_2_inner_tris_adj_to_the_same_outer_tri(vvr::Mesh mesh, int ind1, int ind2, math::vec &com_pt)
{
	//Step 1 : for each tri of the two find its vertices
	std::vector<vvr::Triangle> tris = mesh.getTriangles(); 
	std::vector<math::vec> v1(0);
	std::vector<math::vec> v2(0);

	v1.push_back(tris[ind1].v1());
	v2.push_back(tris[ind2].v1());

	v1.push_back(tris[ind1].v2());
	v2.push_back(tris[ind2].v2());

	v1.push_back(tris[ind1].v3());
	v2.push_back(tris[ind2].v3());

	//Step 2 :find the common vert
	//there are 3 cases:
	if(v1[0].Equals(v2[0]) || v1[0].Equals(v2[1]) || v1[0].Equals(v2[2]))
		com_pt = v1[0];
	else if(v1[1].Equals(v2[0]) || v1[1].Equals(v2[1]) || v1[1].Equals(v2[2]))
		com_pt = v1[1];
	else if(v1[2].Equals(v2[0]) || v1[2].Equals(v2[1]) || v1[2].Equals(v2[2]))////////////if to else if is not needed in the last one
		com_pt = v1[2];

}

void Adjacency_of_tri_region::find_inds_of_verts_adjac_to_common_segments(vvr::Mesh mesh, std::vector<int> inds_of_common_verts_of_bound_tris, std::vector<int> &adj_to_seg_verts_inds)
{
	//Step 1 : initialise 

	//adj_to_seg_verts_inds
	for(unsigned int p = 0 ; p < mesh_indices_of_outer_boundary_tris.size() ; p++)
		adj_to_seg_verts_inds.push_back(-1);
	
	//adj_to_seg_verts_inds
	adj_to_seg_verts_inds.clear();

	for(unsigned int p = 0 ; p < mesh_indices_of_outer_boundary_tris.size() ; p++)
	{
		//Step 2 : check if outer tri is type 1 or type 2 or type 3
		if(num_of_adj_inner_tris_for_each_outer_bound_tri[p] == 1)
		//TYPE 1
		{
			//Step 3 : find tri's common verts with inner tri from inds_of_common_verts_of_bound_tris
			//for each outer tri in inds_of_common_verts_of_bound_tris there are 6  elements of vector --> 3 pairs of inds of common vertices
			//if there is adjacency with only one inner tri then the 3rd, 4rth, 5th and 6th element of vector is (-1,-1)
			int vi1 = inds_of_common_verts_of_bound_tris[6 * p];
			int vi2 = inds_of_common_verts_of_bound_tris[6 * p + 1];

			find_ind_of_vert_adjac_to_segment(vi1, vi2, mesh_indices_of_outer_boundary_tris[p], mesh, adj_to_seg_verts_inds);
		}
		else if(num_of_adj_inner_tris_for_each_outer_bound_tri[p] == 2)
		{
			//TYPE 2 
			//Step 4 : find common vert of two inner tris that are adjacent to the outer one
			math::vec com_pt;
			find_common_vert_of_2_inner_tris_adj_to_the_same_outer_tri(mesh,mesh_indices_of_adj_tris_of_outer_boundary_tris[3 * p], mesh_indices_of_adj_tris_of_outer_boundary_tris[3 * p + 1], com_pt);

			//Step 5 : find vert of outer bound tri not equal to com_pt
			std::vector<vvr::Triangle> tris = mesh.getTriangles();
			vvr::Triangle tri = tris[mesh_indices_of_outer_boundary_tris[p]];
			int ind_of_adj;
			if(!tri.v1().Equals(com_pt))
				ind_of_adj = tri.vi1;
			else if(!tri.v2().Equals(com_pt))
				ind_of_adj = tri.vi2;
			else if(!tri.v3().Equals(com_pt))
				ind_of_adj = tri.vi3;
			
			//Step 5 : save results to adj_to_seg_verts_inds vector
			adj_to_seg_verts_inds.push_back(ind_of_adj);

		}
	}

	

}

void Adjacency_of_tri_region::find_data_about_bound_tris_adjacency(vvr::Mesh mesh, std::vector<int> &inds_of_common_verts_of_bound_tris, std::vector<int> &adj_to_seg_verts_inds)///for each outer tri i will have two or four indices of common vertices
{
	//Step 1 : find common verts of boundary tris
	find_common_verts_of_boundary_tris(mesh,inds_of_common_verts_of_bound_tris);
	
	
	
	//step 2 : find indices of adjacent vertices to common vertices
	find_inds_of_verts_adjac_to_common_segments(mesh, inds_of_common_verts_of_bound_tris, adj_to_seg_verts_inds);


}


void Adjacency_of_tri_region::organize_data_of_adjacency_of_outer_boundary_tris(std::vector<int> inds_of_common_verts_of_bound_tris, std::vector<int> inds_of_verts_adj_to_com_segm)
{
	type1_of_bound_region_tris.clear();
	type2_of_bound_region_tris.clear();
	mesh_indices_to_bound_tris_adj_1_vector.clear();
	mesh_indices_to_bound_tris_adj_2_vector.clear();
	//for every outer bound tri
	for(unsigned int i = 0 ; i < mesh_indices_of_outer_boundary_tris.size() ; i++)
	{
		//Step 1 : check if it is of type 1 or type 2 --> it has one or two adjacent inner tris
		//TYPE 1
		if(num_of_adj_inner_tris_for_each_outer_bound_tri[i] == 1)
		{
			//Step 2 : create an instance of Bound_tris_adj_1
			Bound_tris_adj_1 tri;

			//Step 3 : fill the data needed
			tri.common_vi1 = inds_of_common_verts_of_bound_tris[6 * i];
			tri.common_vi2 = inds_of_common_verts_of_bound_tris[6 * i + 1];
			tri.adj_to_com_seg = inds_of_verts_adj_to_com_segm[i];

			//Step 4 : push_back data to appropriate vectors
			mesh_indices_to_bound_tris_adj_1_vector.push_back(mesh_indices_of_outer_boundary_tris[i]);
			type1_of_bound_region_tris.push_back(tri);

		}
		//TYPE 2
		else if(num_of_adj_inner_tris_for_each_outer_bound_tri[i] == 2)/////
		{
			//Step 2 : create an instance of Bound_tris_adj_1
			Bound_tris_adj_2 tri;

			//Step 3 : fill the data needed
			tri.common_vi1 = inds_of_common_verts_of_bound_tris[6 * i];
			tri.common_vi2 = inds_of_common_verts_of_bound_tris[6 * i + 1];
			tri.common_vi3 = inds_of_common_verts_of_bound_tris[6 * i + 2];
			tri.common_vi4 = inds_of_common_verts_of_bound_tris[6 * i + 3];
			tri.adj_to_com_seg_arbitrary = inds_of_verts_adj_to_com_segm[i];

			//Step 4 : push_back data to appropriate vectors
			mesh_indices_to_bound_tris_adj_2_vector.push_back(mesh_indices_of_outer_boundary_tris[i]);
			type2_of_bound_region_tris.push_back(tri);
		
		}
	
	}

}


void Adjacency_of_tri_region::handle_adjacency_of_boundary_tris(vvr::Mesh mesh, Dual_Graph * dual_graph)
{
	//Step 1 : find indices of inner, outer boundary tris, adjacent tris to outer boundary tris
	find_inner_boundary_triangles(dual_graph);
	find_outer_boundary_triangles(dual_graph);
	find_inds_of_adj_tris_of_outer_bound_tris(dual_graph);

	std::vector<int> inds_of_common_verts_of_bound_tris(0); 
	std::vector<int> inds_of_verts_adj_to_seg(0);
	
	//Step 1 : find adjacency data of outer bound tris
	find_data_about_bound_tris_adjacency(mesh, inds_of_common_verts_of_bound_tris, inds_of_verts_adj_to_seg);

	//Step 2 : organise data to structs vectors
	organize_data_of_adjacency_of_outer_boundary_tris(inds_of_common_verts_of_bound_tris,inds_of_verts_adj_to_seg);
}

bool Adjacency_of_tri_region::get_type_1_tris_info_vectors(std::vector<Bound_tris_adj_1> &type1, std::vector<int> &mesh_indices_of_boundary_type_1)
{
	if(type1_of_bound_region_tris.empty())
		return false;
	else
	{
		type1 = type1_of_bound_region_tris;
		mesh_indices_of_boundary_type_1 = mesh_indices_to_bound_tris_adj_1_vector;
		return true;
	}
}
	
bool Adjacency_of_tri_region::get_type_2_tris_info_vectors(std::vector<Bound_tris_adj_2> &type2, std::vector<int> &mesh_indices_of_boundary_type_2)
{
	if(type2_of_bound_region_tris.empty())
		return false;
	else
	{
		type2 = type2_of_bound_region_tris;
		mesh_indices_of_boundary_type_2 = mesh_indices_to_bound_tris_adj_2_vector;
		return true;
	}
}

void Adjacency_of_tri_region::draw(vvr::Mesh m_model)
{
	//! Draw inner boundary tris
	std::vector<int> index = mesh_indices_of_inner_boundary_tris;
	std::vector<vvr::Triangle> tris = m_model.getTriangles();
	for(unsigned int i = 0; i < index.size() ; i++)
	{
		vvr::Triangle3D picked_tri( tris[index[i]].v1().x, tris[index[i]].v1().y, tris[index[i]].v1().z, tris[index[i]].v2().x, tris[index[i]].v2().y, tris[index[i]].v2().z, tris[index[i]].v3().x, tris[index[i]].v3().y, tris[index[i]].v3().z, vvr::Colour::magenta);
		picked_tri.draw();
	}

	//! Draw outer boundary tris
	index = mesh_indices_of_outer_boundary_tris;
	tris = m_model.getTriangles();
	for(unsigned int i = 0; i < index.size() ; i++)
	{
		vvr::Triangle3D picked_tri( tris[index[i]].v1().x, tris[index[i]].v1().y, tris[index[i]].v1().z, tris[index[i]].v2().x, tris[index[i]].v2().y, tris[index[i]].v2().z, tris[index[i]].v3().x, tris[index[i]].v3().y, tris[index[i]].v3().z, vvr::Colour::yellow);
		picked_tri.draw();
	}

	//! Draw outer boundary tris type1 white
	/*index = mesh_indices_to_bound_tris_adj_1_vector;
	tris = m_model.getTriangles();
	for(unsigned int i = 0; i < index.size() ; i++)
	{
		vvr::Triangle3D picked_tri( tris[index[i]].v1().x, tris[index[i]].v1().y, tris[index[i]].v1().z, tris[index[i]].v2().x, tris[index[i]].v2().y, tris[index[i]].v2().z, tris[index[i]].v3().x, tris[index[i]].v3().y, tris[index[i]].v3().z, vvr::Colour::white);
		picked_tri.draw();
	}

	//! Draw outer boundary tris type2 white
	index = mesh_indices_to_bound_tris_adj_2_vector;
	tris = m_model.getTriangles();
	for(unsigned int i = 0; i < index.size() ; i++)
	{
		vvr::Triangle3D picked_tri( tris[index[i]].v1().x, tris[index[i]].v1().y, tris[index[i]].v1().z, tris[index[i]].v2().x, tris[index[i]].v2().y, tris[index[i]].v2().z, tris[index[i]].v3().x, tris[index[i]].v3().y, tris[index[i]].v3().z, vvr::Colour::);
		picked_tri.draw();
	}

	//! Draw outer boundary tris type3 blue
	index = mesh_indices_of_adj_tris_of_outer_boundary_tris;
	tris = m_model.getTriangles();
	for(unsigned int i = 0; i < index.size() ; i++)
	{
		if(mesh_indices_of_adj_tris_of_outer_boundary_tris[i] != -1)
		{
			vvr::Triangle3D tri( tris[index[i]].v1().x, tris[index[i]].v1().y, tris[index[i]].v1().z, tris[index[i]].v2().x, tris[index[i]].v2().y, tris[index[i]].v2().z, tris[index[i]].v3().x, tris[index[i]].v3().y, tris[index[i]].v3().z, vvr::Colour::blue);
			tri.draw();
		}
	}*/
	//////////////erase it afterwards
	/*for(unsigned int i = 0 ; i < type1_of_bound_region_tris.size() ; i++)
	{
		
		//! Draw common verts red
		vvr::Point3D(m_model.getVertices()[type1_of_bound_region_tris[i].common_vi1].x, m_model.getVertices()[type1_of_bound_region_tris[i].common_vi1].y, m_model.getVertices()[type1_of_bound_region_tris[i].common_vi1].z, vvr::Colour::red).draw();
		vvr::Point3D(m_model.getVertices()[type1_of_bound_region_tris[i].common_vi2].x, m_model.getVertices()[type1_of_bound_region_tris[i].common_vi2].y, m_model.getVertices()[type1_of_bound_region_tris[i].common_vi2].z, vvr::Colour::red).draw();
		
		//! Draw adj verts black
		vvr::Point3D(m_model.getVertices()[type1_of_bound_region_tris[i].adj_to_com_seg].x, m_model.getVertices()[type1_of_bound_region_tris[i].adj_to_com_seg].y, m_model.getVertices()[type1_of_bound_region_tris[i].adj_to_com_seg].z, vvr::Colour::black).draw();
		
	}

	for(unsigned int i = 0 ; i < type2_of_bound_region_tris.size() ; i++)
	{
		
		//! Draw common verts red
		vvr::Point3D(m_model.getVertices()[type2_of_bound_region_tris[i].common_vi1].x, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi1].y, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi1].z, vvr::Colour::red).draw();
		vvr::Point3D(m_model.getVertices()[type2_of_bound_region_tris[i].common_vi2].x, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi2].y, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi2].z, vvr::Colour::red).draw();
		vvr::Point3D(m_model.getVertices()[type2_of_bound_region_tris[i].common_vi3].x, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi3].y, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi3].z, vvr::Colour::red).draw();
		vvr::Point3D(m_model.getVertices()[type2_of_bound_region_tris[i].common_vi4].x, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi4].y, m_model.getVertices()[type2_of_bound_region_tris[i].common_vi4].z, vvr::Colour::red).draw();

		//! Draw adj verts black
		vvr::Point3D(m_model.getVertices()[type2_of_bound_region_tris[i].adj_to_com_seg_arbitrary].x, m_model.getVertices()[type2_of_bound_region_tris[i].adj_to_com_seg_arbitrary].y, m_model.getVertices()[type2_of_bound_region_tris[i].adj_to_com_seg_arbitrary].z, vvr::Colour::black).draw();
		
	}
		*/
}

#endif
