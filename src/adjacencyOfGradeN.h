/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/


#ifndef _ADJACENCY_GRADEN
#define _ADJACENCY_GRADEN

#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
//#include "symmetriceigensolver3x3.h"
#include <VVRScene/scene.h>
#include <stdlib.h>
#include "dualGraphOfMesh.h"
#include "rayCastPicker.h"

class Adjacency_of_triangle
{
	int ind_of_tri_picked;
	int grade;
	std::vector<int> indices_of_adj_tris;

public:
	Adjacency_of_triangle(int ind_of_tri);
	void find_indices_of_adj_tris_of_grade_n(Dual_Graph *dual_graph);
	void draw_adj_tris(vvr::Mesh m_model);
	bool get_indices_of_adjac_tris(std::vector<int> &copy_vector);
	void get_grade(int &gr);
	void set_adjacency_of_tri_region(std::vector<int> new_adj_tri);

private:
	void get_the_grade_from_user();
	void find_indices_of_adj_tris_of_triangle(std::vector<int> inds_of_dual_graph, int ind_of_tri, std::vector<int> &candidate_inds_of_adj_tris_to_push_back);
	bool ind_already_exists(int ind_of_candidate_adj_tri_to_push_back);
	void save_inds_of_adj_tris_in_vec(std::vector<int> temp_vec_4_inds_of_adj_tris);


	
};

Adjacency_of_triangle::Adjacency_of_triangle(int ind_of_tri)
{
	//initialization of fields of class
	indices_of_adj_tris.clear();
	ind_of_tri_picked = ind_of_tri;
	get_the_grade_from_user();
}

void Adjacency_of_triangle::get_the_grade_from_user()
{
	std::cout << "Type the grade of adjacency of tri picked from ray cast...";
	int n;
	std::cin >> n;
	while(n < 0)
	{
		std::cout << "Number must be an integer! Type again..";
		std::cin >> n;
	}
	grade = n;

}

void Adjacency_of_triangle::set_adjacency_of_tri_region(std::vector<int> new_adj_tri)
{
	indices_of_adj_tris = new_adj_tri;
}

bool Adjacency_of_triangle::ind_already_exists(int ind_of_candidate_adj_tri)
{
	//edw epidexetai veltiwsh --> ligoteres sugkriseis
	//Step 1 : set an 'exists' flag = false
	bool exists = false;
	for(unsigned int k = 0 ; k < indices_of_adj_tris.size() ; k++)
	{
		//if ind_of_candidate_adj_tri == -1, this means that there isn't an index to an adjacent tri 
		//so by setting exists = true, it won't be saved afterwards(while 'save_inds_of_adj_tris_in_vec' is executed) to 
		//the indices_of_adj_tris vector 
		if((indices_of_adj_tris[k] == ind_of_candidate_adj_tri) || ind_of_candidate_adj_tri == -1)
		{
			exists = true;
			break;
		}
	}

	//Step 2 : if exists = false, then return false
	//if not do the opposite
	if(exists)
		return true;
	else 
		return false;
}
	
void Adjacency_of_triangle::save_inds_of_adj_tris_in_vec(std::vector<int> temp_vec_4_inds_of_adj_tris)
{
	//Step 1 : for every tri of the three adjacent ones(may be less), check if it already exists in indices_of_adj_tris vector
	for(unsigned int i = 0 ; i < temp_vec_4_inds_of_adj_tris.size() ; i++)
	{
		//Step 2 : if not, push it back to vector indices_of_adj_tris
		if(!ind_already_exists(temp_vec_4_inds_of_adj_tris[i]))
			indices_of_adj_tris.push_back(temp_vec_4_inds_of_adj_tris[i]);
	}


}

void Adjacency_of_triangle::find_indices_of_adj_tris_of_triangle(std::vector<int> indices_of_dual_graph, int ind_of_tri_picked, std::vector<int> &candidate_inds_of_adj_tris_to_push_back)
{
	//Step1 : find the indices of adjacent tris of triangle picked from indices_of_dual_graph vector
	candidate_inds_of_adj_tris_to_push_back.push_back(indices_of_dual_graph[3 * ind_of_tri_picked]);
	candidate_inds_of_adj_tris_to_push_back.push_back(indices_of_dual_graph[3 * ind_of_tri_picked + 1]);
	candidate_inds_of_adj_tris_to_push_back.push_back(indices_of_dual_graph[3 * ind_of_tri_picked + 2]);

	//Step 2 : find from the above indices, the ones that don't already exist in indices_of_adj_tris field of class Adjacency_of_triangle
	//and save them to vector
	save_inds_of_adj_tris_in_vec(candidate_inds_of_adj_tris_to_push_back);
}



void Adjacency_of_triangle::find_indices_of_adj_tris_of_grade_n(Dual_Graph * dual_graph)
{

	//Step 1 : initializations
	indices_of_adj_tris.push_back(ind_of_tri_picked);
	std::vector<int> inds_of_adj_tris_of_dual_graph(0);
	dual_graph->get_indices_of_adjacent_tris(inds_of_adj_tris_of_dual_graph);


	int init_size = 0;
	int final_size = indices_of_adj_tris.size();

	//Step 2 : I assume that i is the level of grade of adjacency
	for(unsigned int i = 0 ; i < grade ; i++)
	{
	
	    //Step 3 : init_size and final_size represent the indices of triangles that are related with each level(i level)
		for(unsigned int u = init_size ; u < final_size ; u++)
		{
			std::vector<int> candidate_inds_of_adj_tris_to_push_back(0);

			//Step 4 : for each tri of i level find its adjacent triangles and push them back to indices_of_adjacent_tris vector if they don't already exist
			find_indices_of_adj_tris_of_triangle(inds_of_adj_tris_of_dual_graph, indices_of_adj_tris[u], candidate_inds_of_adj_tris_to_push_back);
		
		}
		//Step 5 : update the init_size, final_size to show to indices of tris of level i + 1
		init_size = final_size;
		final_size = indices_of_adj_tris.size();
	}
	
}

void Adjacency_of_triangle::draw_adj_tris(vvr::Mesh m_model)
{
	//Step 1 : draw the picked tri red
	vvr::Triangle3D picked_tri( (m_model.getTriangles())[indices_of_adj_tris[0]].v1().x,(m_model.getTriangles())[indices_of_adj_tris[0]].v1().y,(m_model.getTriangles())[indices_of_adj_tris[0]].v1().z, (m_model.getTriangles())[indices_of_adj_tris[0]].v2().x,(m_model.getTriangles())[indices_of_adj_tris[0]].v2().y,(m_model.getTriangles())[indices_of_adj_tris[0]].v2().z, (m_model.getTriangles())[indices_of_adj_tris[0]].v3().x, (m_model.getTriangles())[indices_of_adj_tris[0]].v3().y, (m_model.getTriangles())[indices_of_adj_tris[0]].v3().z, vvr::Colour::red);
	picked_tri.draw();

	//Step 2 : draw all other cyan
	for(unsigned int i = 1 ; i < indices_of_adj_tris.size() ; i++)
	{
		vvr::Triangle3D tri( (m_model.getTriangles())[indices_of_adj_tris[i]].v1().x,(m_model.getTriangles())[indices_of_adj_tris[i]].v1().y,(m_model.getTriangles())[indices_of_adj_tris[i]].v1().z, (m_model.getTriangles())[indices_of_adj_tris[i]].v2().x,(m_model.getTriangles())[indices_of_adj_tris[i]].v2().y,(m_model.getTriangles())[indices_of_adj_tris[i]].v2().z, (m_model.getTriangles())[indices_of_adj_tris[i]].v3().x, (m_model.getTriangles())[indices_of_adj_tris[i]].v3().y, (m_model.getTriangles())[indices_of_adj_tris[i]].v3().z, vvr::Colour::cyan);
		tri.draw();
	}

}

bool Adjacency_of_triangle::get_indices_of_adjac_tris(std::vector<int> &copy_vector)
{
	//return true is vector is not empty, after having copied the indices_of_adj_tris vector to copy_vector
	//else return false
	if(indices_of_adj_tris.empty())
	{
		std::cout << "Indices_of_adjacent_tris vector is empty!";
		return false;
	}
	else
	{
		copy_vector = indices_of_adj_tris;
		return true;
	}

}


//den vazw periorismous gia na kalupsw thn katastash pou den exei dhmiourghthei to instance ths klasshs
//auto tha to elegksw me to adjacency_instance_exists(thn private metavlhth ths Mesh3DScene) prin thn klhsh ths parakatw methodou 
void Adjacency_of_triangle::get_grade(int &gr)
{
	gr = grade;	
}



#endif
