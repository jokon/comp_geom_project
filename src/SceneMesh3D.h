/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/
#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
#include "symmetriceigensolver3x3.h"
#include <VVRScene/scene.h>
#include "dualGraphOfMesh.h"
#include "adjacencyOfGradeN.h"
#include "subdivisionOfRegionOfTriangles.h"
#include "vertexSeperator.h"
#include "adjacencyOfTriRegion.h"
#include "deformationTool.h"

#include "GeoLib.h"

#define FLAG_SHOW_AXES                   1
#define FLAG_SHOW_WIRE                   2
#define FLAG_SHOW_SOLID                  4
#define FLAG_SHOW_NORMALS                8
#define FLAG_SHOW_PLANE                 16
#define FLAG_SHOW_AABB                  32
#define FLAG_SHOW_DUAL_GRAPH            64
#define FLAG_RAY_CAST_PICKING          128
#define FLAG_SHOW_ADJACENT_TRIS        256
#define FLAG_SUBDIVIDE_REGION_OF_MESH  512
#define FLAG_VERTEX_SEPERATOR         1024

void findCenterMass(std::vector<vec> &vertices, vec &cm);
void findAABB(std::vector<vec> &vertices, vvr::Box3D &aabb);
void alignOriginTo(std::vector<vec> &vertices, const vec &cm);





class Mesh3DScene : public vvr::Scene
{
public:
    Mesh3DScene();
    const char* getName() const { return "3D Scene"; }
    void keyEvent(unsigned char key, bool up, int modif) override;
    void arrowEvent(vvr::ArrowDir dir, int modif) override;
	//erwt2
	void find_ray(Ray &ray, int m_x, int m_y);

private:
    void draw() override;
    void reset() override;
    void resize() override;
	void mousePressed(int x, int y, int modif) override;
	void mouseReleased(int x, int y, int modif) override;
	void mouseMoved(int x, int y, int modif) override;
   // void mouseWheel(int dir, int modif) override;
	void Inits();
    vec pick_Origin();
	
private:
	bool adjacency_instance_exists;
	bool ray_cast_pick_instance_exists;
	//erwt 3a 
	int i;

    int m_style_flag;
    //float m_plane_d;
    vvr::Canvas2D m_canvas;
    vvr::Colour m_obj_col;
    vvr::Mesh m_model_original, m_model;
	vvr::Box3D m_aabb;
    math::vec m_center_mass;
    //math::Plane m_plane;
	int m_x, m_y;
	//erwt 1
	Dual_Graph *dual_graph;
	//erwt 2a
	Ray_cast_picking_tool  *ray_cast_picking_tool;
	Ray ray;
	//erwt 2b
	Adjacency_of_triangle *adjacency_of_tri;
	//erwt 3a
	Processing_of_triangles_tool *subdivision_tool;
	Adjacency_of_tri_region *adjac_of_tri_region;
	//erwt 4
	Vertex_seperator_of_mesh_region *vertex_sep_tool;
	//erwt 5
	
	
};
