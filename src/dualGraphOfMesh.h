/*
*	This file is part of 3D Mesh Processing Project.
*	This project is free for non-commercial use.
*	Copyright © 2016 Ioanna Kontou
*/

#ifndef _DUAL_GRAPH 
#define _DUAL_GRAPH

#include <VVRScene/canvas.h>
#include <VVRScene/mesh.h>
#include <VVRScene/settings.h>
#include <VVRScene/utils.h>
#include <MathGeoLib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <set>
//#include "symmetriceigensolver3x3.h"
#include <VVRScene/scene.h>
#include <stdlib.h>



//kwdikas gia ulopoihsh erwthma1oApallaktikhs

class Dual_Graph
{
	std::vector<vec> vertices;
	std::vector<int> indices_of_adjacent_tris;

private:
	void find_verts(std::vector<vvr::Triangle> mesh_tris);
	void find_indices(std::vector<vvr::Triangle> mesh_tris);
public:
	Dual_Graph();	
	bool get_vertices(std::vector<vec> &verts);
	bool get_indices_of_adjacent_tris(std::vector<int> &ind_of_adjac_tris);
	void find_dual_graph(std::vector<vvr::Triangle> mesh_tris);
	void draw();

};

// Member functions definitions including constructor
Dual_Graph::Dual_Graph()
{
	//Constructor --> just erase existing data from vectors
	vertices.clear();
	indices_of_adjacent_tris.clear();
}

bool Dual_Graph::get_vertices(std::vector<vec> &verts)
{
	//if field vertices is not empty: 
	//1)content of field vertices will be copied to verts vector
	//2)true will be returned, 
	//else:
	//1)message asking the user to create the dual graph will be printed
	//2)false will be returned
	
	if(vertices.empty())
	{
		std::cout << " Dual graph of mesh is not created! ";
		std::cout << "Press C to create dual graph, then D to draw dual graph...";
		return false;
	}
	else
	{
		verts = vertices;
		return true;
	}
		
}

bool Dual_Graph::get_indices_of_adjacent_tris(std::vector<int> &ind_of_adjac_tris)
{
	//if field indices_of_adjacent_tris is empty: 
	if(vertices.empty())
	{
		//Step1 : message asking the user to create the dual graph will be printed
		std::cout << " Dual graph of mesh is not created! ";
		std::cout << "Press C to create dual graph, then D to draw dual graph...";
		
		//Step2 : false will be teturned
		return false;
	}
	//if field indices_of_adjacent_tris is not empty: 
	else
	{
		//Step1 : content of field will be copied to ind_of_adjac_tris parameter
		ind_of_adjac_tris = indices_of_adjacent_tris;
		
		//Step2 : true will be returned
		return true;
	}

}

void Dual_Graph::find_verts(std::vector<vvr::Triangle> mesh_tris)
{
	vertices.clear();
	for(int j = 0 ; j < mesh_tris.size(); j++)
	{
		//Step1 : find the centroid for every triangle of the mesh 
		vec centroid = mesh_tris[j].getCenter();
		
		//Step2 : push it back to field vertices
		vertices.push_back(centroid);
	}
}

void Dual_Graph::find_indices(std::vector<vvr::Triangle> mesh_tris)
{
	indices_of_adjacent_tris.clear();
	//Step 1 : allocate mem space equal to mesh_tris_size*3, type int --> buffer
	//there, the indices of adjacent tris (buffer[3 * index], buffer[3* index + 1], buffer[3 * index + 2]) will be saved for triangle  tri[index] of mesh 
	int *buffer = (int *)malloc(3*mesh_tris.size()*sizeof(int));///int64_t;
	if(buffer == NULL )
	{
		std::cout << "Error while allocating memory";
		exit(1);
	}
	
	//allocate mem , every element stored in num_of_adjac_tris_saved_for_each_tri shows the number of adjacent tris saved on buffer for each tri of mesh 
	int *num_of_adjac_tris_saved_for_each_tri = (int *) calloc(mesh_tris.size(), sizeof(int));//int64_t

	//Step 2 : initialize the the buffer with value -1
	//By doing this, if there is a triangle that does not have exactly 3 adjacent tris(e.g. there is a hole)
	//in its position to 'indices_of_adjacent_tris' vector, the value will be -1
	for(int i = 0 ; i < 3 * mesh_tris.size() ; i++)
	{
		buffer[i] = -1;
	}

	//make a copy of mesh_tris
	std::vector<vvr::Triangle> tris = mesh_tris;
	
	for(int i = 0 ; i < mesh_tris.size(); i++)
	{
		//Step 3 : find vetices of mesh_tris[i]
		vec v1 = mesh_tris[i].v1();
		vec v2 = mesh_tris[i].v2();
		vec v3 = mesh_tris[i].v3();

		//pairs of vertices defining edges of triangle : (v1,v2), (v1,v3), (v2,v3)
		//check for each pair of vertices if there is another triangle of the mesh with the same vertices
		//(vi1,vi2), (vi1,vi3), (vi2,vi3) --> i compare vertices

		//Step 4 : erase the first triangle of the vector 'tris' so as not to compare it with itself and avoid pairs in indices_of_adjacent_tris that already exist
		tris.erase(tris.begin());
		for(int j = 0; j < tris.size() ; j++)
		//for(int j = 0; j < mesh_tris.size() ; j++)
		{
			//if (j==i) continue;
			vec vi1 = tris[j].v1();
			vec vi2 = tris[j].v2();
			vec vi3 = tris[j].v3();

			//Step 5 : find triangle's vertices
			//i compare it with vertices and not indices of vertices, because there might be a vertex with two different normals --> two different indice

		 
			//Step 6 : check for same edges between the two triangles
			if(((v1.Equals( vi1) || v1.Equals(vi2) || v1.Equals(vi3)) && (v2.Equals(vi1) || v2.Equals(vi2) || v2.Equals( vi3))) 
				|| ((v1.Equals(vi1) || v1.Equals(vi2) || v1.Equals(vi3)) && (v3.Equals(vi1) || v3.Equals(vi2) || v3.Equals(vi3))) 
				|| ((v2.Equals(vi1) || v2.Equals(vi2) || v2.Equals(vi3)) && (v3.Equals(vi1) || v3.Equals(vi2) || v3.Equals(vi3))))
			{
				//Step 7 : if yes, then temporarily save indices of triangles to 'buffer'
				//and the number of adjacent tris for every tri of mesh to 'num_of_adjac_tris_saved_for_each_tri' 
				num_of_adjac_tris_saved_for_each_tri[i] += 1;
				num_of_adjac_tris_saved_for_each_tri[i + j + 1] += 1;

				buffer[3 * i + num_of_adjac_tris_saved_for_each_tri[i] - 1] /*=j;*/= i + j + 1;
				buffer[3 * (i + j + 1) + num_of_adjac_tris_saved_for_each_tri[i + j + 1] - 1] = i;
			}
		}
	}

	//Step 8 : push_back indices of adjacent tris stored in buffer to field indices_of_adjacent_tris
	for(int i = 0 ; i < 3 * mesh_tris.size() ; i += 3 )
	{
		indices_of_adjacent_tris.push_back(buffer[i]);
		indices_of_adjacent_tris.push_back(buffer[i + 1]);
		indices_of_adjacent_tris.push_back(buffer[i + 2]);
	}

	//Step 9 : free the mem used above
	free(buffer);
	free(num_of_adjac_tris_saved_for_each_tri);
}

void Dual_Graph::find_dual_graph(std::vector<vvr::Triangle> mesh_tris)
{
	//Step 1 : find vertices
	find_verts(mesh_tris);

	//Step 2 : find indices of adjacent tris of dual graph
	find_indices(mesh_tris);
		
}

void Dual_Graph::draw()
{
	for(int i = 0 ; i < vertices.size() ; i++)
	{
		//Step 1 : pt0 is the centroid of the triangle of mesh with index i that connects with the
		//centroids of its adjacent tris pt1,pt2 and pt3(if it exists) --> this does not exist when there are holes in the object
		vec pt0 = vertices[i];
		if(indices_of_adjacent_tris[3*i] != -1 )
		{
			vec pt1 = vertices[indices_of_adjacent_tris[3*i]];
			vvr::LineSeg3D(pt0.x, pt0.y, pt0.z, pt1.x, pt1.y, pt1.z, vvr::Colour::cyan).draw();
		}
		if(indices_of_adjacent_tris[3*i+1] != -1)
		{
			vec pt2 = vertices[indices_of_adjacent_tris[3*i+1]];
			vvr::LineSeg3D(pt0.x, pt0.y, pt0.z, pt2.x, pt2.y, pt2.z, vvr::Colour::cyan).draw();
		}
		if(indices_of_adjacent_tris[3*i+2] != -1)
		{
			vec pt3 = vertices[indices_of_adjacent_tris[3*i+2]];
			vvr::LineSeg3D(pt0.x, pt0.y, pt0.z, pt3.x, pt3.y, pt3.z, vvr::Colour::cyan).draw();
		
		}
		
	}
}





#endif
