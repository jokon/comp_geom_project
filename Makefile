.SILENT:

.PHONY: all
all: release

.PHONY: release
release:
	cmake -H. -Bbuild/release -DCMAKE_BUILD_TYPE=Release
	cmake --build build/release -- -j3

.PHONY: debug
debug:
	cmake -H. -Bbuild/debug -DCMAKE_BUILD_TYPE=Debug
	cmake --build build/debug -- -j3

.PHONY: clean
clean:
	rm -rf bin
	rm -rf build

.PHONY: install_vvr
install_vvr:
	bash resources/scripts/install_vvr.sh

.PHONY: uninstall_vvr
uninstall_vvr:
	rm -f /usr/local/lib/libGeoLib.so
	rm -f /usr/local/lib/libMathGeoLib.a
	rm -f /usr/local/lib/libVVRScene.so
	rm -rf /usr/local/include/Geolib
	rm -rf /usr/local/include/MathGeolib
	rm -rf /usr/local/include/VVRScene

